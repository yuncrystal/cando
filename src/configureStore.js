import { applyMiddleware, compose, createStore } from 'redux'
import reducers from './reducers';
import { TOKEN } from './types/common';
import thunk from 'redux-thunk';

export default function configureStore(){
    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

    const middlewares = [thunk]
    return createStore(reducers, 
        {
            auth : {
                token: localStorage.getItem(TOKEN)
            }
        }, composeEnhancers(
        applyMiddleware(...middlewares)
    ));
}
