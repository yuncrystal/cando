import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux'
import { NavLink, Link } from 'react-router-dom'
import { Row, Col, Avatar, Menu, Dropdown } from 'antd';
import { withTranslation } from 'react-i18next';
import { logout } from '../../actions/auth';
import configMenu from './menuConfig'

// style styles/header.scss




const Header = (props) => {
    const { t, i18n } = props;
    const changeLanguage = lng => {
        i18n.changeLanguage(lng);
    };

    const menu = (
        <Menu>
            <Menu.Item>
                <Link to='/my-center'>{t('menu.user_center')}</Link>
            </Menu.Item>
            <Menu.Item onClick={() => {
                props.logout();
            }}>
            {t('menu.log_out')}
            </Menu.Item>
        </Menu>
    );

    return (
        <div>
            <Row className='header-container'>
                <Col className='logo' span={6}>
                    <img src={process.env.PUBLIC_URL + '/img/logo/logo-190911-1.png'} alt='cando-logo'/>
                </Col>
                <Col span={10} className='nav-page'>
                    {
                        configMenu.map((menu) => (
                            <NavLink exact={ menu.key === 1 ? true : false} key={menu.key} to={menu.path} activeClassName="nav-selected">
                                {t(`menu.${menu.contextId}`)}
                            </NavLink>
                        ))
                    }
                </Col>
                <Col span={8}>
                    {
                        props.auth.token
                            ?
                            <div className='nav-auth-container'>
                                <Dropdown overlay={menu} trigger={['click','hover']}>
                                    <a href="#"><Avatar className='avatar' icon="user" /></a>
                                </Dropdown>



                                <div className='language-btn'>
                                    {
                                        i18n.language === 'en'
                                            ? <Fragment><span>EN </span> / <span className='change-language' onClick={() => changeLanguage('zh')}> 中</span></Fragment>
                                            : <Fragment><span className='change-language' onClick={() => changeLanguage('en')}>EN </span> / <span> 中</span></Fragment>
                                    }

                                </div>
                            </div>
                            :
                            <div className='nav-auth-container'>
                                <div className='nav-btn signin'>
                                    <Link to='/signin' >
                                        {t('auth.signin')}
                                    </Link>
                                </div>
                                <div className='nav-btn signup'>
                                    <Link to='/signup' >
                                        {t('auth.signup')}
                                    </Link>
                                </div>
                                <div className='language-btn'>
                                    {
                                        i18n.language === 'en'
                                            ? <Fragment><span>EN </span> / <span className='change-language' onClick={() => changeLanguage('zh')}> 中</span></Fragment>
                                            : <Fragment><span className='change-language' onClick={() => changeLanguage('en')}>EN </span> / <span> 中</span></Fragment>
                                    }

                                </div>
                            </div>
                    }

                </Col>
            </Row>


        </div>
    )
}

function mapStateToProps(state) {
    return {
        auth: state.auth
    }
}

export default compose(
    connect(mapStateToProps,{logout}),
    withTranslation(),
)(Header)
