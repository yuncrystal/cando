const configMenu = [
  {
      key: 1,
      contextId: 'home',
      path: '/',
  },
//   {
//       key: 2,
//       contextId: 'description',
//       path: '/introduce',
//   },
  {
      key: 3,
      contextId: 'trace_manage',
      path: '/trace_manage',
  },
  {
      key: 4,
      contextId: 'place_order',
      path: '/place_order',
  }
]

export default configMenu;