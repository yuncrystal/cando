import React, { Fragment } from 'react';
import { Link } from 'react-router-dom'
import configMenu from './menuConfig';
import { useTranslation } from 'react-i18next';
// style styles/footer.scss



const Footer = (props) => {
    const { t } = useTranslation();

    const goBack = () => {
        window.scrollTo({
            top: 0,
            left: 0,
            behavior: 'smooth'
        })
    }

    return (
        <div className='row'>
            <div className='footer-container'>
                <div className='logos'>
                    <i className="fab fa-twitter"></i>
                    <i className="fab fa-facebook-f"></i>
                    <i className="fab fa-google-plus-g"></i>
                    <i className="fab fa-pinterest-p"></i>
                </div>
                <div className='links flex-row'>
                    <Fragment>
                        {
                            configMenu.map((menu) => (
                                <Fragment key={menu.key}>
                                    <Link to={menu.path} >
                                        {t(`menu.${menu.contextId}`)}
                                    </Link>
                                    <i className="fas fa-circle"></i>
                                </Fragment>


                            ))
                        }
                        <Link to='/signin' >
                            {t('auth.signin')}
                        </Link>
                        <i className="fas fa-circle"></i>
                        <Link to='/signup' >
                            {t('auth.signup')}
                        </Link>
                        <i className="fas fa-circle"></i>
                        <Link>
                            {t('text.contact')} 02 97008955
                        </Link>
                    </Fragment>
                    <div className='up-arrow' onClick={goBack}>
                        <i className="fas fa-chevron-up"></i>
                    </div>
                </div>
            </div>

        </div>
    )

}

export default Footer;