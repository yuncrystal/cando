
import React from 'react';
import {connect} from 'react-redux';
import { Form, Input, Avatar } from 'antd';
import {getUserInfo} from '../../actions/auth'
import { useTranslation } from 'react-i18next';
import requireAuth from '../../HOC/requireAuth';

const UserInfo = (props) => {
    const {auth} = props;
    const { t } = useTranslation();

    if(!auth.account){
        props.getUserInfo();
    }

    const formItemLayout = {
        labelCol: {
            xs: { span: 24 },
            sm: { span: 4 },
        },
        wrapperCol: {
            xs: { span: 24 },
            sm: { span: 20 },
        },
    };

    return (
        <div className='main-container'>
            <div className='user-info'>
                <div className='main-title'>
                    {t('MyCenterMenu.subMenu1.content.item1')}
                </div>
                <div >
                    <Form {...formItemLayout}>
                        <Form.Item
                            label={t('userInfo.avator')} >
                            <Avatar style={{ backgroundColor: '#87d068' }} icon="user" />
                        </Form.Item>

                        <Form.Item
                            label={t('userInfo.username')} >
                            <Input value={auth.account ? auth.account.username : ''} disabled />
                        </Form.Item>
                        <Form.Item
                            label={t('userInfo.mobile')} >
                            <Input value={auth.account ? auth.account.mobile : ''} disabled />
                        </Form.Item>
                    </Form>
                </div>
            </div>
        </div>
    )
}

function mapStateToProps(state) {
    return {
        auth: state.auth
    }
}


export default requireAuth(connect(mapStateToProps,{getUserInfo})(UserInfo));