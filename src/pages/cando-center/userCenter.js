import React from 'react';

import requireAuth from '../../HOC/requireAuth';
import Menu from '../cando-components/menu';
import { Route } from "react-router-dom";

import UserInfo from './userInfo';
import ChangePassword from './changePassword';
import NewAddress from './newAddress';
import AddressDetail from './addressDetail';
import ManageAddress from './manageAddress';
// import Suggestion from './suggestion';
//  use style userCenter

const menuConfig = [
    {
        key: 'subMenu1',
        title: 'MyCenterMenu.subMenu1.title',
        icon: 'fa-cog',
        content: [
            {
                key: '7',
                item: 'MyCenterMenu.subMenu1.content.item1',
                url: ''
            },
            {
                key: '8',
                item: 'MyCenterMenu.subMenu1.content.item2',
                url: '/change-password'
            }
        ]
    },
    {
        key: 'subMenu2',
        title: 'MyCenterMenu.subMenu2.title',
        icon: 'fa-map-marked-alt',
        content: [
            {
                key: '9',
                item: 'MyCenterMenu.subMenu2.content.item1',
                url: '/new-address'
            },
            {
                key: '10',
                item: 'MyCenterMenu.subMenu2.content.item2',
                url: '/manage-address'
            }
        ]
    },
    // {
    //     key: 'subMenu3',
    //     title: 'MyCenterMenu.subMenu3.title',
    //     content: [
    //         {
    //             key: '11',
    //             item: 'MyCenterMenu.subMenu3.content.item1',
    //             url: '/suggestion'
    //         }
    //     ]
    // }
]

class UserCenter extends React.Component {
    constructor(props) {
        super(props);
        this.state = { menuKey: '7' };
    }
    componentWillMount() {
        const { location } = this.props;
        let currentUrl = location.pathname;
        if (currentUrl.includes('change-password')) {
            this.setState({
                menuKey: '8'
            })
        } else if (currentUrl.includes('new-address')) {
            this.setState({
                menuKey: '9'
            })
        } else if (currentUrl.includes('manage-address') || currentUrl.includes('address/')) {
            this.setState({
                menuKey: '10'
            })
        }
    }

    render() {
        const { match, history } = this.props;
        return (
            <div className='main-layout'>
                <Menu
                    menuConfig={menuConfig}
                    menuTitle={'MyCenterMenu.title'}
                    defaultOpenKeys={['subMenu1', 'subMenu2', 'subMenu3']}
                    defaultSelectedKeys={[this.state.menuKey]}
                    match={match}
                    history={history}
                />
                <Route exact path={`${match.path}/`} component={UserInfo} />
                <Route exact path={`${match.path}/change-password`} component={ChangePassword} />
                <Route exact path={`${match.path}/new-address`} component={NewAddress} />
                <Route exact path={`${match.path}/address/:id`} component={AddressDetail} />
                <Route exact path={`${match.path}/manage-address`} component={ManageAddress} />
                {/* <Route exact path={`${match.path}/suggestion`} component={Suggestion} /> */}
    
            </div>
        )
    }
}

export default (requireAuth(UserCenter));