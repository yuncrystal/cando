
import React, { useState } from 'react';
import { API_URL } from '../../api/apiUrl';
import candoAPI from '../../api/candoAPI';
import useResources from '../../hookResource/resourceAddressList';
import { message } from 'antd';
import { useTranslation } from 'react-i18next';
import { Link } from "react-router-dom";
import DeletePackageModal from '../cando-components/deletePackageModal';
import EmptyList from '../cando-components/emptyList'
const ManageAddress = (props) => {

    const { t } = useTranslation();

    const [modalVisibilityDelete, setModalVisibilityDelete] = useState(false);
    const [deleteAddressId, setDeleteAddressId] = useState(null);
    const [listChanged, setListChanged] = useState(0);

    const closeModalDelete = () => {
        setModalVisibilityDelete(false);
    }

    const deleteAddress = async () => {
        try {
            await candoAPI.delete(API_URL.ADDRESS.All_ADDRESS+deleteAddressId+'/');
            setModalVisibilityDelete(false)
            setListChanged(listChanged+1)
            message.success(t('address.delete_address_success'), 4);
        } catch (e) {
            message.error(t('address.delete_address_failed') + ":" + e.response.data,4);
        }
    }

    const addressList = useResources(API_URL.ADDRESS.All_ADDRESS,listChanged, () => {
        message.error(t('address.get_address_error'), 4);
    });
    return (
        <div className='main-container'>
            <div className='user-info'>
                <div className='main-title'>
                    {t('MyCenterMenu.subMenu2.content.item2')}
                </div>
            </div>
            <div className='address-list'>
                <div className='list-header'>
                    <div className='list-receiver center'>{t('address.receiver')}</div>
                    <div className='list-phone center'>{t('address.phone')}</div>
                    <div className='list-address center'>{t('address.address')}</div>
                    <div className='list-action center'>{t('address.action')}</div>
                    {/* <div className='list-default center'></div> */}
                </div>
                {
                    addressList.length
                        ?
                        addressList.map((address, index) => {
                            return (
                                <div className='list-row' key={index}>
                                    <div className='list-receiver center'>{address.receiver}</div>
                                    <div className='list-phone center'>{address.phone}</div>
                                    <div className='list-address center'>{address.address}</div>
                                    <div className='list-action center'>
                                        <div className='alter'>
                                            <Link to={`/my-center/address/${address.id}`}>
                                                {t('address.alter')}
                                            </Link>

                                        </div>
                                        <div className='delete'
                                            onClick={() => {
                                                setModalVisibilityDelete(true);
                                                setDeleteAddressId(address.id)
                                            }}>
                                            {t('address.delete')}
                                        </div>
                                    </div>
                                    {/* <div className='list-default center'>{t('address.set_to_default')}</div> */}
                                </div>

                            )
                        })

                        :
                        <EmptyList content={t('address.no_address')} linkContent={t('address.add_address')} link={'/my-center/new-address'}/>
                }
            </div>
            <DeletePackageModal
                closeModal={closeModalDelete}
                modalVisibility={modalVisibilityDelete}
                deletePackage={deleteAddress}
                title={t('address.delete_address_title')}
                content={t('address.delete_address_content')} />
        </div>
    )
}

export default ManageAddress;