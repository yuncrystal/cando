import React from 'react';
import { Form, Input, message, Button } from 'antd';
import { useTranslation } from 'react-i18next';
import candoAPI from '../../api/candoAPI';
import { API_URL } from '../../api/apiUrl';

const ChangePassword = (props) => {
    const { getFieldDecorator, getFieldsValue, validateFields, getFieldsError, resetFields } = props.form;
    const { t } = useTranslation();

    const formItemLayout = {
        labelCol: {
            xs: { span: 24 },
            sm: { span: 6 },
        },
        wrapperCol: {
            xs: { span: 24 },
            sm: { span: 18 },
        },
    };

    const changePassword = async (e) => {
        e.preventDefault();
        validateFields((err, values) => {
            if (err) {
                console.log('err: ', err);
            }
        });
        const errors = getFieldsError();
        if (!errors.current_password && !errors.new_password && !errors.re_new_password) {
            try {
                await candoAPI.post(API_URL.AUTH.CHANGE_PASSWORD, getFieldsValue());
                message.success(t('change_password.change_password_susccess'), 4);
                resetFields();
            } catch (e) {
                message.error(t('change_password.change_password_failed') + ":" + Object.values(e.response.data)[0], 4);
            }
        }
    }


    return (
        <div className='main-container'>
            <div className='user-info'>
                <div className='main-title'>
                    {t('address.create_address')}
                </div>
                <div >
                    <Form {...formItemLayout} onSubmit={changePassword}>
                        <Form.Item
                            label={t('change_password.old_password')} >
                            {getFieldDecorator('current_password', {
                                rules: [{ required: true, message: t('change_password.old_password_error') }],
                            })(
                                <Input placeholder={t('change_password.old_password_tip')} type="password" />
                            )}

                        </Form.Item>

                        <Form.Item
                            label={t('change_password.new_password')} >
                            {getFieldDecorator('new_password', {
                                rules: [{ required: true, message: t('change_password.new_password_error') },
                                { min: 8, message: t('change_password.old_password_error2') }],
                            })(
                                <Input placeholder={t('change_password.new_password_tip')} type="password" />
                            )}

                        </Form.Item>
                        <Form.Item
                            label={t('change_password.new_password_again')} >
                            {getFieldDecorator('re_new_password', {
                                rules: [
                                    {
                                        validator: (rules, value, callback) => {
                                            if (!value) {
                                                callback(t('change_password.new_password_again_error'))
                                            } else {
                                                if (value !== getFieldsValue(['new_password']).new_password) {
                                                    callback(t('singup.confirm_password_not_same_error'))
                                                } else {
                                                    callback()
                                                }
                                            }

                                        }
                                    }],
                            })(
                                <Input placeholder={t('change_password.new_password_again_tip')} type="password" />
                            )}

                        </Form.Item>

                        <Form.Item>
                            <Button type="primary" htmlType="submit" value="small">
                                {t('button.confirm')}</Button>
                        </Form.Item>

                    </Form>
                </div>
            </div>
        </div>
    )
}

export default Form.create({ name: 'change_passward' })(ChangePassword);