
import React from 'react';
import { Form, Input, message, Button } from 'antd';
import { useTranslation } from 'react-i18next';
import Loading from '../cando-error/loading'
import candoAPI from '../../api/candoAPI';
import { API_URL } from '../../api/apiUrl';
import useResources from '../../hookResource/resourceList';
import validator from 'validator';


const AddressDetail = (props) => {
    const { getFieldDecorator } = props.form;
    const { t } = useTranslation();

    const formItemLayout = {
        labelCol: {
            xs: { span: 24 },
            sm: { span: 4 },
        },
        wrapperCol: {
            xs: { span: 24 },
            sm: { span: 20 },
        },
    };

    const addressId = props.match.params.id;

    const addressDetail = useResources(API_URL.ADDRESS.All_ADDRESS + addressId + '/', () => {
        message.error(t('address.get_address_detail_error'), 8);
    });


    const changeAddressDetail = async (e) => {
        e.preventDefault();
        props.form.validateFields(['receiver', 'phone', 'address'], (err, values) => {
            if (err) {
                console.log('err: ', err);
            }
        });
        const errors = props.form.getFieldsError();
        if (!errors.address && !errors.phone && !errors.receiver) {
            try {
                await candoAPI.put(API_URL.ADDRESS.CREATE_ADDRESS + addressId + '/', props.form.getFieldsValue());
                message.success(t('address.change_address_detail_success'), 4);
                // props.form.resetFields();
            } catch (e) {
                message.error(t('address.change_address_detail_error'), 4);
            }
        }
    }


    return (
        <div className='main-container'>
            {
                addressDetail
                    ? <div className='user-info'>
                        <div className='main-title'>
                            {t('address.alter_address')}
                        </div>
                        <div >
                            <Form {...formItemLayout} onSubmit={changeAddressDetail}>
                                <Form.Item
                                    label={t('address.receiver')} >
                                    {getFieldDecorator('receiver', {
                                        initialValue: addressDetail.receiver,
                                        rules: [
                                            // { required: true, message: t('address.receiver_error') },
                                            {
                                                validator: (rules, value, callback) => {
                                                    if (!value) {
                                                        callback(t('address.receiver_error'))
                                                    } else {
                                                        if(/^[A-Za-z][A-Za-z\s]*\b[A-Za-z]+$/.test(value)){
                                                            callback();
                                                        }else {
                                                            callback(t('address.receiver_formate_error'))
                                                        }
                                                    }
                                                }
                                            }
                                        ],
                                    })(
                                        <Input placeholder={t('address.receiver_tip')} />
                                    )}

                                </Form.Item>

                                <Form.Item
                                    label={t('address.phone')} >
                                    {getFieldDecorator('phone', {
                                        initialValue: addressDetail.phone,
                                        rules: [
                                            {
                                                validator: (rules, value, callback) => {
                                                    if (!value) {
                                                        callback(t('address.phone_error'))
                                                    } else {
                                                        if (!validator.isMobilePhone(value, ['zh-CN', 'en-AU'])) {
                                                            callback(t('singup.phone_tip_error'))
                                                        } else {
                                                            callback();
                                                        }
                                                    }
                                                }
                                            }],
                                    })(
                                        <Input placeholder={t('address.phone_tip')} />
                                    )}

                                </Form.Item>
                                <Form.Item
                                    label={t('address.address')} >
                                    {getFieldDecorator('address', {
                                        initialValue: addressDetail.address,
                                        rules: [{ required: true, message: t('address.address_error') }],
                                    })(
                                        <Input placeholder={t('address.address_tip')} />
                                    )}

                                </Form.Item>

                                {/* <Form.Item>
                                    {getFieldDecorator('default-address', {
                                        valuePropName: 'checked',
                                        initialValue: false,
                                    })(
                                        <Checkbox>{t('address.set_default_address')}</Checkbox>
                                    )}
                                </Form.Item> */}

                                <Form.Item>
                                    <Button type="primary" htmlType="submit" value="small">
                                        {t('address.save')}</Button>
                                </Form.Item>

                            </Form>
                        </div>
                    </div>
                    : <Loading />
            }

        </div>
    )
}

export default Form.create({ name: 'change_address' })(AddressDetail);