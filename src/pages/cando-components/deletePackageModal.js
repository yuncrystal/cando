import React from 'react';
import PropTypes from 'prop-types';
import Modal from 'react-modal';
import { useTranslation } from 'react-i18next';


const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        width: '615px',
        height: '245px',
        transform: 'translate(-50%, -50%)'
    }
};
Modal.setAppElement('#root')


const DeletePackageModal = (props) => {
    const { t } = useTranslation();
    const { closeModal, modalVisibility, deletePackage ,title,content} = props;


    return (
        <Modal
            isOpen={modalVisibility}
            // onAfterOpen={this.afterOpenModal}
            onRequestClose={closeModal}
            style={customStyles}
            contentLabel="Delete Package"
        >
            <div className='package-modal'>
                <div className='close-modal-icon'>
                    <span className='add-package-icon' onClick={closeModal}>
                        <i className="fas fa-times"></i>
                    </span>
                </div>
                <div className='package-info'>
                    <div className='modal-title'>
                        {title}
                    </div>
                    <div>
                        {content}
                    </div>
                    <div className='delete-operation'>
                        <div className='grey-btn' onClick={closeModal} style={{marginRight:20}}>{t('button.cancel')}</div>
                        <div className='blue-btn' onClick={()=>deletePackage()} >{t('button.confirm')}</div>
                    </div>


                </div>

            </div>

        </Modal>
    )
}


DeletePackageModal.propTypes = {
    closeModal: PropTypes.func.isRequired,
    deletePackage: PropTypes.func.isRequired,
    modalVisibility: PropTypes.bool.isRequired,
    title: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired,
};
export default DeletePackageModal;


