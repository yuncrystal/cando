import React from 'react';
import PropTypes from 'prop-types';
import Modal from 'react-modal';
import { Form, Input, Button } from 'antd';
import { useTranslation } from 'react-i18next';


const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        width: '615px',
        height: '415px',
        transform: 'translate(-50%, -50%)'
    }
};
Modal.setAppElement('#root')


const AddPackageModal = (props) => {
    const { t } = useTranslation();
    const { closeModal, modalVisibility, addPackage, form } = props;
    const { getFieldDecorator, getFieldsValue, getFieldsError } = form;

    const handleSubmit = (e) => {
        e.preventDefault();
        props.form.validateFields((err, values) => {
            if (err) {
                console.log('err: ', err);
            }
        });

        const errors = getFieldsError();
        if (!errors.orderitem_content && !errors.tracking_number) {
            addPackage(getFieldsValue());
        }
    }

    return (
        <Modal
            isOpen={modalVisibility}
            // onAfterOpen={this.afterOpenModal}
            onRequestClose={closeModal}
            style={customStyles}
            contentLabel="Add Package"
        >
            <div className='package-modal'>
                <div className='close-modal-icon'>
                    <span className='add-package-icon' onClick={closeModal}>
                        <i className="fas fa-times"></i>
                    </span>
                </div>
                <div className='package-info'>
                    <div className='modal-title'>
                        {t('add_package.add_package_info')}
                    </div>

                    <Form layout='vertical' onSubmit={handleSubmit}>
                        <Form.Item
                            label={t('add_package.package_info')}>
                            {getFieldDecorator('orderitem_content', {
                                rules: [{ required: true, message: t('add_package.package_info_error') }],
                            })(
                                <Input placeholder={t('add_package.package_info_tip')} />
                            )}

                        </Form.Item>
                        <Form.Item
                            label={t('add_package.package_num')} >
                            {getFieldDecorator('tracking_number', {
                                rules: [{ required: true, message: t('add_package.package_num_error') }],
                            })(
                                <Input placeholder={t('add_package.package_num_tip')} />
                            )}
                        </Form.Item>
                        <Form.Item className='flex-row center-h'>
                            <Button type="primary" htmlType="submit" className="blue-btn">
                                {t('button.confirm')}</Button>
                        </Form.Item>
                    </Form>
                </div>

            </div>

        </Modal>
    )
}


AddPackageModal.propTypes = {
    closeModal: PropTypes.func.isRequired,
    addPackage: PropTypes.func.isRequired,
    modalVisibility: PropTypes.bool.isRequired,
};
export default Form.create({ name: 'add_package_form' })(AddPackageModal);


