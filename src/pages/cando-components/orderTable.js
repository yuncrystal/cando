
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { message } from 'antd';
import { useTranslation } from 'react-i18next';
import moment from 'moment'
import candoAPI from '../../api/candoAPI';
import { API_URL } from '../../api/apiUrl';
import { Link } from "react-router-dom";
import DeletePackageModal from '../cando-components/deletePackageModal';
import PhotoModal from '../cando-components/photoModal';
import AddPackageModal from '../cando-components/addPackageModal'
import qs from 'qs';


const OrderTable = ({ order, completeOrder, updateList }) => {
    const { t } = useTranslation();
    const { rp_orderitems, status, express_service_code} = order;

    const [modalVisibilityDelete, setModalVisibilityDelete] = useState(false);
    const [modalVisibilityPhoto, setModalVisibilityPhoto] = useState(false);
    const [modalVisibilityPackage, setModalVisibilityPackage] = useState(false);
    const [photoUrl, setPhotoUrl] = useState('');

    const closeModalPackage = () => {
        setModalVisibilityPackage(false)
    }
    const closeModalDelete = () => {
        setModalVisibilityDelete(false);
    }

    const openModalPhoto = (imgUrl) => {
        setPhotoUrl(imgUrl)
        setModalVisibilityPhoto(true);
    }

    const closeModalPhoto = () => {
        setModalVisibilityPhoto(false);
    }

    const RenderOrderState = ({ status }) => {
        let title = '';
        if (status === 20 || status === 40) {
            title = t('orderList.wait_patient')
        } else if (status === 50) {
            title = t('orderList.package_coming')
        }
        return (
            <div style={{ paddingBottom: 10 }}>
                {title}
            </div>
        );
    }


    const addPackage = async (data) => {
        try {
            await candoAPI.post(API_URL.ORDER.ADD_PACKAGE, {
                order: order.id,
                orderitem_content: data.orderitem_content,
                tracking_number: data.tracking_number,
            });
            closeModalPackage();
            updateList()
            message.success(t('add_package.add_package_success'), 4);
        } catch (error) {
            message.error(t('add_package.add_package_failed'), 4);
        }
    }


    const deleteOrder = async () => {
        try {
            await candoAPI.post(API_URL.ORDER.DELETE, qs.stringify({
                order_id: order.id
            }));
            updateList()
            setModalVisibilityDelete(false)
            message.success(t('orderList.delete_order_success'), 4);
        } catch (e) {
            message.error(t('orderList.delete_order_failed') + ":" + e.response.data, 4);
        }
    }



    return (
        <div className='order-table'>
            <div className='table-header'>
                <div className='table-row'>
                    <div className='table-data table-col-1'> {t('orderList.package_info')}</div>
                    <div className='table-data table-col-2'> {t('orderList.packages')}</div>
                    <div className='table-data table-col-3'>
                        {t('orderList.operation')}
                        {
                            status === 10
                                ? <i className="fas fa-trash-alt order-delete" onClick={() => setModalVisibilityDelete(true)}></i>
                                : null
                        }
                    </div>
                </div>
            </div>
            <div className='table-body'>
                <div className='table-row'>
                    <div className='table-data table-col-1'>
                        <div>
                            <div className='order-info'>
                                <div>{t('orderDetail.order_number')}：{order.id} ({order.express_service_name})</div>
                                <div>{t('orderDetail.order_create_time')}：{moment(order.create_date).format('YYYY-MM-DD hh:mm:ss')}</div>
                                <div>{t('text.weight')}：{order.box_weight} g</div>
                                <div>{t('text.volume_weight')}：{order.box_volume_weight} g</div>
                                <div>{t('orderDetail.order_fee')}：{t('aud')}{order.price}</div>
                                <div>{t('orderDetail.order_status')}：{t(`orderDetail.status.${order.status}`)}</div>
                                <div className='volume-weight-notification'>
                                    <p>{t('text.volume_weight_notification')}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='table-data table-col-2'>
                        {
                            rp_orderitems.length > 0
                                ?
                                rp_orderitems.map((item) => {
                                    const { is_received, tracking_number, create_date, image, orderitem_content } = item;
                                    return (
                                        <div className='item-row' key={tracking_number}>
                                            <div className='item-img'>
                                                {
                                                    is_received && image
                                                        ?
                                                        <img src={image} alt="item-img" onClick={() => openModalPhoto(image)} />
                                                        :
                                                        <span className='img-icon'>
                                                            <i className="fas fa-images"></i>
                                                        </span>
                                                }
                                            </div>
                                            <div className='item-details'>
                                                <div>{t('orderList.packages')}：{orderitem_content}</div>
                                                <div>{t('orderList.package_num')}：{tracking_number}</div>
                                                <div>{t('orderList.package_create_time')}：{moment(create_date).format('YYYY-MM-DD HH:mm:ss')}</div>
                                            </div>
                                        </div>
                                    )
                                })
                                : <div className='no-package'>{t('orderList.no_package')}</div>
                        }
                    </div>


                    <div className='table-data table-col-3'>
                        <div>
                            <div className='order-operation'>
                                <RenderOrderState status={order.status} />

                                <Link to={`/order/${order.id}`}>
                                    {t('orderList.order_detail')}
                                </Link>

                                {
                                    order.status === 10
                                        ? <div onClick={() => setModalVisibilityPackage(true)} className='confirm-btn'>{t('orderDetail.add_package')}</div>
                                        : null
                                }
                                {
                                    order.status === 50
                                        ? <div onClick={() => completeOrder(order.id)} className='confirm-btn'>{t('orderList.confirm_received')}</div>
                                        : null
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <DeletePackageModal
                closeModal={closeModalDelete}
                modalVisibility={modalVisibilityDelete}
                deletePackage={deleteOrder}
                title={t('orderList.delete_order_title')}
                content={t('orderList.delete_order_content')} />
            <PhotoModal
                closeModal={closeModalPhoto}
                modalVisibility={modalVisibilityPhoto}
                img={photoUrl}
            />
            <AddPackageModal
                closeModal={closeModalPackage}
                modalVisibility={modalVisibilityPackage}
                addPackage={addPackage} />
        </div>
    )
}

OrderTable.propTypes = {
    order: PropTypes.object.isRequired,
    updateList: PropTypes.func
};


export default OrderTable;