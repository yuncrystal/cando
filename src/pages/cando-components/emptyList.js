
import React from 'react';
import PropTypes from 'prop-types';
import { Link } from "react-router-dom";

const EmptyList = ({ content, link, linkContent }) => {
    return (
        <div className='empty-list'>
            <div className='title'>
                {content}
            </div>
            {
                link
                    ? <Link to={link}>
                        {linkContent}
                    </Link>

                    : null
            }


        </div>
    )

}
EmptyList.propTypes = {
    content: PropTypes.string.isRequired,
    link: PropTypes.string,
    linkContent: PropTypes.string,
};


export default EmptyList;