import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { withTranslation } from 'react-i18next'
import { Menu } from 'antd'
import { NavLink } from 'react-router-dom'
import { getAllOrder } from '../../actions/order'

const SubMenu = Menu.SubMenu

const Menus = props => {
  const {
    t,
    menuConfig,
    defaultOpenKeys,
    defaultSelectedKeys,
    match,
    menuTitle,
    updateList
  } = props
  const [orderNum, setOrderNum] = useState({})
  useEffect(() => {
    let unmounted = false
    async function calculateOrderNum() {
      let orderNums = {
        '10': 0,
        '15': 0,
        '20': 0,
        '30': 0,
        '40': 0,
        '50': 0,
        '100': 0,
        '110': 0,
        all: 0,
        in_transit: 0
      }
      const allOrder = await getAllOrder()
      allOrder.data.map(order => {
        orderNums[order.status] = orderNums[order.status] + 1
        orderNums['all'] = orderNums['all'] + 1
      })
      orderNums['in_transit'] =
        orderNums['30'] + orderNums['40'] + orderNums['50']
      if (!unmounted) setOrderNum(orderNums)
    }
    calculateOrderNum()

    return () => {
      unmounted = true
    }
  }, [updateList])

  return (
    <div className='menu'>
      <div className='menu-title'>{t(menuTitle)}</div>
      <Menu
        style={{ width: 256 }}
        defaultSelectedKeys={defaultSelectedKeys}
        defaultOpenKeys={defaultOpenKeys}
        mode='inline'
        // onClick={(e)=>{
        //     console.log('click item:',e)
        // }}
      >
        {menuConfig.map(subMenu => {
          return (
            <SubMenu
              key={subMenu.key}
              title={
                <span>
                  <i
                    className={`fas ${subMenu.icon}`}
                    style={{ paddingRight: 10 }}
                  ></i>
                  <span className='menu-sub'>{t(subMenu.title)}</span>
                </span>
              }
            >
              {subMenu.content.map(item => {
                return (
                  <Menu.Item key={item.key}>
                    <NavLink to={`${match.url}${item.url}`}>
                      {t(item.item)}{' '}
                      {item.status
                        ? `(${
                            orderNum[item.status] ? orderNum[item.status] : 0
                          })`
                        : null}
                    </NavLink>
                  </Menu.Item>
                )
              })}
            </SubMenu>
          )
        })}
      </Menu>
    </div>
  )
}

Menus.propTypes = {
  menuConfig: PropTypes.array.isRequired,
  menuTitle: PropTypes.string.isRequired,
  defaultOpenKeys: PropTypes.array.isRequired,
  defaultSelectedKeys: PropTypes.array.isRequired,
  match: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  updateList: PropTypes.number
}

export default withTranslation()(Menus)
