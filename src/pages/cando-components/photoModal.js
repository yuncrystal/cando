import React from 'react';
import PropTypes from 'prop-types';
import Modal from 'react-modal';


const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        // width: '615px',
        // height: '245px',
        transform: 'translate(-50%, -50%)'
    }
};
Modal.setAppElement('#root')


const PhotoModal = (props) => {
    const { closeModal, modalVisibility, img } = props;


    return (
        <Modal
            isOpen={modalVisibility}
            onRequestClose={closeModal}
            style={customStyles}
        >
            <div>
                <img src={img} alt=''/>
            </div>


        </Modal>
    )
}

PhotoModal.propTypes = {
    closeModal: PropTypes.func.isRequired,
    modalVisibility: PropTypes.bool.isRequired,
    img: PropTypes.string.isRequired,
   
};
export default PhotoModal;