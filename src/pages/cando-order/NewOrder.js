
import React, { useState, useEffect } from 'react';
import requireAuth from '../../HOC/requireAuth';
import { API_URL } from '../../api/apiUrl';
import { connect } from 'react-redux'
import candoAPI from '../../api/candoAPI';
import useResources from '../../hookResource/resourceList';
import { useTranslation } from 'react-i18next';
import { message, Form, Checkbox, Radio, Button, Input, Tooltip } from 'antd';
import { Link } from "react-router-dom";
import { compose } from 'redux'
import { getUserInfo } from '../../actions/auth'
import validator from 'validator';

const PlaceOrder = (props) => {

    useEffect(() => {
        props.getUserInfo()
    }, [])
    const { t, i18n } = useTranslation();
    const { getFieldDecorator, validateFields, getFieldsError, getFieldsValue } = props.form;
    const [choosedAddressId, setChoosedAddressId] = useState(null);
    const [chooseEasyGo, setChooseEasyGo] = useState(false);

    const { account } = props.auth;
    // console.log('mobile:',mobile);
    const serviceList = useResources(API_URL.SERVICE.All_SERVICE, () => {
        message.error(t('newOrder.get_services_error'), 5);
    });

    const receiverList = useResources(API_URL.ADDRESS.All_ADDRESS, () => {
        message.error(t('newOrder.get_address_error'), 5);
    });
    const enabledServices = serviceList ? serviceList.filter(service => service.is_enable) : [];
    const changeAddress = (e) => {

        setChoosedAddressId(e.target.value)
    }

    const changeService = (e) => {
        const choosedService = serviceList.find((service) => {
            return service.id === e.target.value;
        })
        if (choosedService.service_code === 'easy_go' || choosedService.service_code === 'easy_go_2') {
            setChooseEasyGo(true)

        } else {
            setChooseEasyGo(false)
        }
    }
    const handleSubmit = async (e) => {
        e.preventDefault();
        if (!receiverList.length > 0) {
            validateFields(['express_service', 'receiver', 'repackage_service', 'phone', 'address', 'agree_term'], (err, values) => {
                if (err) {
                    // console.log('err: ', err);
                }
            });
            const errors = getFieldsError();
            if (!errors.express_service && !errors.receiver && !errors.repackage_service && !errors.phone && !errors.address && !errors.agree_term) {
                const values = getFieldsValue();

                if (chooseEasyGo) {
                    values.address = '12 works place Milperra NSW 2214';
                    values.receiver = account ? account.username : '';
                    values.phone = account ? account.mobile : ''
                } else {
                    // add address for new users
                    try {
                        await candoAPI.post(API_URL.ADDRESS.CREATE_ADDRESS, {
                            receiver: values.receiver,
                            address: values.address,
                            phone: values.phone,
                        });
                        message.success(t('address.add_address_success'), 4);
                    } catch (e) {
                        // console.log('e:',e.resopnse);
                        message.error(t('address.add_address_error'), 4);
                    }
                }

                try {
                    await candoAPI.post(API_URL.ORDER.CREATE_ORDER, {
                        receiver: values.receiver,
                        address: values.address,
                        phone: values.phone,
                        express_service: values.express_service,
                        repackage: values.repackage_service
                    });
                    message.success(t('newOrder.create_order_success'), 4);
                    message.info(t('newOrder.change_shipping_address_notification'), 4);
                    props.history.push('/trace_manage');

                } catch (e) {
                    // console.log('e:',e.resopnse);
                    message.error(t('newOrder.create_order_failed'), 4);
                }
            }


        } else {
            validateFields(['express_service', 'receiver_detail', 'repackage_service', 'agree_term'], (err, values) => {
                if (err) {
                    // console.log('err: ', err);
                }
            });
            const errors = getFieldsError();
            if (!errors.express_service && !errors.receiver_detail && !errors.agree_term && !errors.repackage_service) {
                const values = getFieldsValue();
                let orderReceiver = {};

                if (chooseEasyGo) {
                    orderReceiver.address = '12 works place Milperra NSW 2214';
                    orderReceiver.receiver = account ? account.username : '';
                    orderReceiver.phone = account ? account.mobile : ''
                } else {
                    orderReceiver = receiverList.find((receiver) => {
                        return receiver.id === values.receiver_detail
                    })
                }


                try {
                    await candoAPI.post(API_URL.ORDER.CREATE_ORDER, {
                        receiver: orderReceiver.receiver,
                        address: orderReceiver.address,
                        phone: orderReceiver.phone,
                        express_service: values.express_service,
                        repackage: values.repackage_service
                    });
                    message.success(t('newOrder.create_order_success'), 4);
                    message.info(t('newOrder.change_shipping_address_notification'), 4);
                    props.history.push('/trace_manage');

                } catch (e) {
                    message.error(t('newOrder.create_order_failed'), 4);
                }
            }
        }
    }



    return (
        <div className='new-order-container'>
            <Form layout='vertical' onSubmit={handleSubmit}>
                <div className='service'>
                    <div className='title'>
                        {t('newOrder.choose_service')}

                    </div>
                    <div className='space-line-blue' />
                    <div className='list'>
                        <Form.Item>
                            {getFieldDecorator('express_service', {
                                rules: [{ required: true, message: t('newOrder.choose_service_tip') }]
                            })(
                                <Radio.Group onChange={changeService}>
                                    {
                                        enabledServices.map((enabledService) => {
                                            return (
                                                <Tooltip trigger={['click']} placement="topLeft" title={i18n.language === 'en' ? enabledService.note_en : enabledService.note_cn}>
                                                    <Radio value={enabledService.id} key={enabledService.id}>
                                                        {enabledService.name}：{i18n.language === 'en' ? <span dangerouslySetInnerHTML={{ __html: enabledService.description_en }} /> : <span dangerouslySetInnerHTML={{ __html: enabledService.description_cn }} />}
                                                    </Radio>
                                                </Tooltip>

                                            )
                                        })
                                    }
                                </Radio.Group>
                            )}

                        </Form.Item>


                    </div>
                    <div className='volume-weight-notification'>
                        <p>{t('text.volume_weight_notification')}</p>
                    </div>
                </div>
                <div className='receiver-detail'>
                    {
                        receiverList && receiverList.length > 0
                            ?
                            <div>
                                <div className='title'>
                                    {t('newOrder.confrim_receiver_detail')}
                                    <Link to={`/my-center/manage-address`}>
                                        {` ${t('newOrder.address_manage')}`}
                                    </Link>
                                </div>
                                <div className='space-line-blue' />
                                <div className='receiver-list'>
                                    <Form.Item>
                                        {getFieldDecorator('receiver_detail', {
                                            rules: [
                                                {
                                                    validator: (rules, value, callback) => {
                                                        if (chooseEasyGo) {
                                                            callback();
                                                        } else {
                                                            if (!value) {
                                                                callback(t('newOrder.receiver_detail_tip'))
                                                            } else {
                                                                callback();
                                                            }

                                                        }

                                                    }
                                                }],
                                        })(
                                            <Radio.Group onChange={changeAddress} disabled={chooseEasyGo}>
                                                {
                                                    receiverList.map((receiver) => {

                                                        return (
                                                            <div className='receiver_address' key={receiver.id} >
                                                                <span style={{ display: choosedAddressId === receiver.id ? 'block' : 'none' }} className='icon'><i className="fas fa-map-marker-alt" style={{ paddingRight: 20 }}></i>{t('newOrder.delivery_to')}</span>
                                                                <Radio value={receiver.id} >
                                                                    {`${receiver.address} (${receiver.receiver} ${t('newOrder.receive')}) ${receiver.phone}`}
                                                                </Radio>
                                                                <span style={{ display: choosedAddressId === receiver.id ? 'block' : 'none' }} className='link'>
                                                                    <Link to={`/my-center/address/${receiver.id}`}>
                                                                        {t('newOrder.change_address')}
                                                                    </Link>
                                                                </span>
                                                            </div>

                                                        )
                                                    })
                                                }
                                            </Radio.Group>
                                        )}

                                    </Form.Item>
                                </div>
                            </div>
                            :
                            <div>
                                <div className='title'>
                                    {t('newOrder.receiver_detail')}
                                </div>
                                <div className='space-line-blue' />
                                <div>
                                    <Form.Item
                                        label={t('newOrder.receiver')} >
                                        {getFieldDecorator('receiver', {
                                            rules: [
                                                {
                                                    validator: (rules, value, callback) => {
                                                        if (chooseEasyGo) {
                                                            callback();
                                                        } else {
                                                            if (!value) {
                                                                callback(t('newOrder.receiver_error'))
                                                            } else {
                                                                callback();
                                                            }
                                                        }
                                                    }
                                                }],
                                            // rules: [{ required: true, message: t('newOrder.receiver_error') }],
                                        })(
                                            <Input placeholder={t('newOrder.receiver_tip')} disabled={chooseEasyGo} />
                                        )}

                                    </Form.Item>
                                    <Form.Item
                                        label={t('newOrder.phone')} >
                                        {getFieldDecorator('phone', {
                                            rules: [
                                                {
                                                    validator: (rules, value, callback) => {
                                                        if (chooseEasyGo) {
                                                            callback();
                                                        } else {
                                                            if (!value) {
                                                                callback(t('newOrder.phone_error'))
                                                            } else {
                                                                if (!validator.isMobilePhone(value, ['zh-CN', 'en-AU'])) {
                                                                    callback(t('singup.phone_tip_error'))
                                                                } else {
                                                                    callback();
                                                                }
                                                            }
                                                        }
                                                    }
                                                }],
                                            // rules: [{ required: true, message: t('newOrder.phone_error') }],
                                        })(
                                            <Input placeholder={t('newOrder.phone_tip')} disabled={chooseEasyGo} />
                                        )}
                                    </Form.Item>
                                    <Form.Item
                                        label={t('newOrder.address')} >
                                        {getFieldDecorator('address', {
                                            rules: [
                                                {
                                                    validator: (rules, value, callback) => {
                                                        if (chooseEasyGo) {
                                                            callback();
                                                        } else {
                                                            if (!value) {
                                                                callback(t('newOrder.address_error'))
                                                            } else {
                                                                callback();
                                                            }
                                                        }
                                                    }
                                                }],
                                            // rules: [{ required: true, message: t('newOrder.address_error') }],
                                        })(
                                            <Input placeholder={t('newOrder.address_tip')} disabled={chooseEasyGo} />
                                        )}
                                    </Form.Item>
                                </div>

                            </div>
                    }
                </div>
                <div className='repackage'>
                    <div className='title'>
                        {t('newOrder.choose_repackage')}
                    </div>
                    <div className='space-line-blue' />
                    <div className='list'>
                        <Form.Item>
                            {getFieldDecorator('repackage_service', {
                                rules: [{ required: true, message: t('newOrder.choose_repackage_tip') }]
                            })(
                                <Radio.Group>
                                    <Radio value={1}>
                                        {t('newOrder.repackage')}
                                    </Radio>
                                    <Radio value={0}>
                                        {t('newOrder.remain_package')}
                                    </Radio>
                                </Radio.Group>
                            )}

                        </Form.Item>
                    </div>

                </div>
                <div className='agree-term'>
                    <Form.Item>
                        {getFieldDecorator('agree_term', {
                            rules: [
                                {
                                    validator: (rules, value, callback) => {
                                        if (!value) {
                                            callback(t('newOrder.term_tip'))
                                        } else {
                                            callback();
                                        }
                                    }
                                }],
                        })(< Checkbox>
                            <div className='content'>
                                <div>{t('newOrder.agree')}</div>
                                <Link to='/terms'>{t('newOrder.term')}</Link>
                            </div>

                        </Checkbox>)}
                    </Form.Item>
                </div>
                <div className="place-order-btn">
                    <Form.Item>
                        <Button htmlType="submit" >
                            {t('newOrder.place_order')}</Button>
                    </Form.Item>
                </div>
            </Form>
        </div >
    )
}

function mapStateToProps(state) {
    return {
        auth: state.auth
    }
}

export default compose(
    requireAuth,
    connect(mapStateToProps, { getUserInfo }),
    Form.create({ name: 'new_order' })
)(PlaceOrder)
