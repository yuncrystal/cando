import React from 'react';
import { compose } from 'redux'
import { connect } from 'react-redux'
import { Row, Button, Form, Input, message, Select } from 'antd';
import * as actions from '../../actions/auth';
import { withTranslation } from 'react-i18next';
import candoAPI from '../../api/candoAPI';
import { API_URL } from '../../api/apiUrl';
import validator from 'validator';
import qs from 'qs';


const RenderSMSBtn = ({ clickNum, disableSMS, getSMSCode, content }) => {

    return (
        <div style={{ width: '40%', background: '#22B0EB' }}>
            <Button style={{ width: '100%' }} onClick={getSMSCode} disabled={disableSMS} className='sms-btn'>{disableSMS ? clickNum + 's' : content}</Button>
        </div>
    )
}

class ForgetPassword extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            disableSMS: false,
            disableSMS2: false,
            clickNum: 60,
            clickNum2: 60
        };
    }

    resetPassword = async (e) => {
        const { getFieldsError, validateFields, getFieldsValue } = this.props.form;
        const { t } = this.props;
        e.preventDefault();
        validateFields((err, values) => {
            if (err) {
                console.log('err:',err);
            }
        });

        const errors = getFieldsError();
        if (!errors.username && !errors.mobile && !errors.password && !errors.code) {
            const values = getFieldsValue(['username', 'mobile', 'password', 'code', 'prefix']);
            const { username, mobile, password, code, prefix } = values;
            try {
                await candoAPI.post(API_URL.AUTH.RESET_PASSWORD, {
                    username: username,
                    password: password,
                    password_repeat:password,
                    code: code,
                    mobile: prefix === '61' ? '+' + prefix + mobile : mobile
                });
                message.success(t('forgetPassword.reset_password_success'), 4);
                this.props.history.push('/signin');
            } catch (e) {
                message.error(t('forgetPassword.reset_password_failed'), 4);
            }
        }
    }


    getUsername = async (type, e) => {
        const { getFieldsError, validateFields, getFieldsValue } = this.props.form;
        const {t} = this.props;
        e.preventDefault();
        validateFields(['mobile'], (err, values) => {
            if (err) {
                console.log('err:', err);
            }
        });
        const errors = getFieldsError();
        if (!errors.mobile) {
            this.setState({
                disableSMS2: true
            })
            let interval = setInterval(() => {
                if (this.state.clickNum2 === 0) {
                    this.setState({
                        disableSMS2: false
                    });
                    clearInterval(interval)
                } else {
                    this.setState({
                        clickNum2: this.state.clickNum2 - 1
                    });
                }
            }, 1000)

            try {
                const values = getFieldsValue(['mobile', 'prefix']);
                const { prefix, mobile } = values;
                await candoAPI.post(API_URL.AUTH.USER_SMS_CODE, qs.stringify({
                    mobile: prefix === '61' ? '+' + prefix + mobile : mobile
                }));
                message.success(t('singup.send_username_success'), 4);
            } catch (e) {
                message.error(t('singup.send_username_failed'), 4);
            }
        }
    }



    getSMSCode = async (type, e) => {
        const { getFieldsError, validateFields, getFieldsValue } = this.props.form;
        const {t} = this.props;
        e.preventDefault();
        validateFields(['mobile'], (err, values) => {
            if (err) {
                console.log('err: ', err);
            }
        });
        const errors = getFieldsError();
        if (!errors.mobile) {
            this.setState({
                disableSMS: true
            })
            let interval = setInterval(() => {
                if (this.state.clickNum === 0) {
                    this.setState({
                        disableSMS: false
                    });
                    clearInterval(interval)
                } else {
                    this.setState({
                        clickNum: this.state.clickNum - 1
                    });
                }
            }, 1000)
            try {
                const values = getFieldsValue(['mobile', 'prefix']);
                const { prefix, mobile } = values;
                await candoAPI.post(API_URL.AUTH.FORGET_SMS_CODE, {
                    mobile: prefix === '61' ? '+' + prefix + mobile : mobile
                });
                message.success(t('singup.send_sms_success'), 4);
            } catch (e) {
                let error = ''
                if (e.response) {
                    error = e.response.data.mobile[0];
                }
                message.error(t('singup.send_sms_failed') + ':' + error, 4);
            }
        }
    }

    render() {
        const { getFieldDecorator, getFieldsValue } = this.props.form;
        const { t } = this.props;
        const { disableSMS, disableSMS2, clickNum, clickNum2 } = this.state;
        const prefixSelector = getFieldDecorator('prefix', {
            initialValue: '61',
        })(
            <Select style={{ width: 125 }}>
                <Select.Option value="61">{t('singup.australia')}+61</Select.Option>
                <Select.Option value="86">{t('singup.china')}+86</Select.Option>
            </Select>,
        );

        return (
            <div className='form-container'>
                <Row className='form-title'>
                    {t('singin.form.forgetpsd')}
                </Row>
                <Row>
                    <Form layout='vertical' onSubmit={this.resetPassword}>
                        <Form.Item label={t('singup.phone')}>
                            {getFieldDecorator('mobile', {
                                rules: [{
                                    validator: (rules, value, callback) => {
                                        if (!value) {
                                            callback(t('singup.phone_tip'))
                                        } else {
                                            const mobile = '+' + getFieldsValue(['prefix']).prefix + value;
                                            if (!validator.isMobilePhone(mobile, ['zh-CN', 'en-AU'])) {
                                                callback(t('singup.phone_tip_error'))
                                            } else {
                                                callback();
                                            }
                                        }
                                    }
                                }]
                            })(<Input addonBefore={prefixSelector} style={{ width: '100%' }} />)}
                        </Form.Item>
                        <Form.Item label={t('singup.verification_code')}>
                            <div className='flex-row'>
                                <div className='verification-code'>
                                    {getFieldDecorator('code', {
                                        rules: [{ required: true, message: t('singup.verification_code_error') }]
                                    })(<Input placeholder={t('singup.verification_code_tip')} />)}
                                </div>
                                <RenderSMSBtn content={t('singup.get_code')} clickNum={clickNum} getSMSCode={(e) => this.getSMSCode('code', e)} disableSMS={disableSMS} t={t} />

                            </div>
                        </Form.Item>
                        <Form.Item label={t('singup.username')}>
                            <div className='flex-row'>
                                <div className='verification-code'>
                                    {getFieldDecorator('username', {
                                        rules: [{ required: true, message: t('singup.username_tip') }]
                                    })(<Input placeholder={t('singup.username_tip')} />)}
                                </div>
                                <RenderSMSBtn content={t('singup.get_username')} clickNum={clickNum2} getSMSCode={(e) => this.getUsername('username', e)} disableSMS={disableSMS2} t={t} />

                            </div>
                        </Form.Item>

                        <Form.Item
                            label={t('singup.password')} >
                            {getFieldDecorator('password', {
                                rules: [{ required: true, message: t('singup.password_error') }],
                            })(
                                <Input placeholder={t('singup.password_tip')} type="password" />
                            )}
                        </Form.Item>
                        <Form.Item
                            label={t('singup.confirm_password')} >
                            {getFieldDecorator('confrom_password', {
                                rules: [
                                    {
                                        validator: (rules, value, callback) => {
                                            if (!value) {
                                                callback(t('singup.confirm_password_error'))
                                            } else {
                                                if (value !== getFieldsValue(['password']).password) {
                                                    callback(t('singup.confirm_password_not_same_error'))
                                                } else {
                                                    callback()
                                                }
                                            }

                                        }
                                    }],
                            })(
                                <Input placeholder={t('singup.confirm_password_tip')} type="password" />
                            )}
                        </Form.Item>
                        <Form.Item>
                            <Button type="primary" htmlType="submit" className="login-form-button">
                                {t('button.confirm')}</Button>
                        </Form.Item>

                    </Form>
                </Row>
            </div>
        )
    }
}


export default compose(
    connect(null, actions),
    withTranslation(),
    Form.create({ name: 'forget_form' })
)(ForgetPassword)
