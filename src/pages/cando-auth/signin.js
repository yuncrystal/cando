import React from 'react';
import { compose } from 'redux'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom';
import { Row, Button, Form, Input, message } from 'antd';
import * as actions from '../../actions/auth';
import { withTranslation } from 'react-i18next';

/* use styles/auth.scss */


const SignIn = (props) => {
    const { t } = props;
    const { getFieldDecorator, getFieldsValue, getFieldsError } = props.form;

    const handleSignIn = (e) => {
        e.preventDefault();
        props.form.validateFields(['username', 'password'], (err, values) => {
            if (err) {
                console.log('err: ', err);
            }
        });

        const errors = getFieldsError();
        if (!errors.username && !errors.password) {
            props.signin(getFieldsValue(),
                () => {
                    props.getUserInfo();
                    props.history.push('/trace_manage');
                },
                () => {
                    message.error(t('singin.responseError'), 8);
                });
        }


    }

    return (
        <div className='form-container'>
            <Row className='form-title'>
                {t('singin.cando_signin')}
            </Row>
            <Row>
                <Form layout='vertical' onSubmit={handleSignIn}>
                    <Form.Item
                        label={t('singin.form.username')} >
                        {getFieldDecorator('username', {
                            rules: [{ required: true, message: t('singin.error.username') }],
                        })(
                            <Input placeholder={t('singin.tip.username')} />
                        )}

                    </Form.Item>
                    <Form.Item
                        label={t('singin.form.password')} >
                        {getFieldDecorator('password', {
                            rules: [{ required: true, message: t('singin.error.password') }],
                        })(
                            <Input placeholder={t('singin.form.password')} type="password" />
                        )}
                    </Form.Item>

                    <Form.Item>
                        <div className='remember-me'>
                            {/* {getFieldDecorator('remember', {
                                valuePropName: 'checked',
                                initialValue: true,
                            })(
                                <Checkbox>{t('singin.form.rememberme')}</Checkbox>
                            )} */}
                            <span>{t('singin.form.unregister')}</span>
                            <Link style={{ paddingRight: 10 }} to='/signup'>{t('singin.form.registernow')}</Link>
                            <Link to='/forget_password'>{t('singin.form.forgetpsd')}</Link>
                        </div>
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" htmlType="submit" className="login-form-button">
                            {t('auth.signin')}</Button>
                    </Form.Item>
                    <Form.Item className='agreement'>
                        {t('singin.form.agreement')}
                    </Form.Item>
                </Form>
            </Row>
        </div>
    )
}



export default compose(
    connect(null, actions),
    withTranslation(),
    Form.create({ name: 'signin_form' })
)(SignIn)
