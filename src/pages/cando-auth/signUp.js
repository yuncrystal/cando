
import React from 'react';
import { compose } from 'redux'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom';
import { Row, Button, Form, Input, Checkbox, message, Select } from 'antd';
import * as actions from '../../actions/auth';
import { withTranslation } from 'react-i18next';
import candoAPI from '../../api/candoAPI';
import { API_URL } from '../../api/apiUrl';
import validator from 'validator';

const RenderSMSBtn = ({ clickNum, disableSMS, getSMSCode, t }) => {

    return (
        <div style={{ width: '40%',background:'#22B0EB' }}>
            <Button style={{ width: '100%' }} onClick={getSMSCode} disabled={disableSMS} className='sms-btn'>{disableSMS ? clickNum + 's' : t('singup.get_code')}</Button>
        </div>
    )
}

class SignUp extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            disableSMS: false,
            clickNum: 60
        };
    }

    handleSingUp = async (e) => {
        const { getFieldsError, validateFields, getFieldsValue } = this.props.form;
        const { t } = this.props;
        e.preventDefault();
        validateFields(['username', 'mobile', 'password', 'confrom_password', 'code', 'agree'], (err, values) => {
            if (err) {
                console.log('err: ', err);
            }
        });

        const errors = getFieldsError();
        if (!errors.username && !errors.mobile && !errors.password && !errors.code && !errors.agree) {
            const values = getFieldsValue(['username', 'mobile', 'password', 'code', 'prefix']);
            const { username, mobile, password, code, prefix } = values;
            try {
                await candoAPI.post(API_URL.AUTH.REGISTER, {
                    username: username,
                    password: password,
                    code: code,
                    mobile: prefix === '61' ? '+' + prefix + mobile : mobile
                });
                message.success(t('singup.register_success'), 4);
                this.props.history.push('/signin');
            } catch (e) {
                let error = '';
                if (e.response.data) {
                    if (e.response.data.username) {
                        error = e.response.data.username[0]
                    }
                }
                message.error(t('singup.register_failed') + ':' + error, 4);
            }
        }
    }


    getSMSCode = async (e) => {
        const { getFieldsError, validateFields, getFieldsValue } = this.props.form;
        const { t } = this.props;
        e.preventDefault();
        validateFields(['mobile'], (err, values) => {
            if (err) {
                console.log('err: ', err);
            }
        });
        const errors = getFieldsError();
        if (!errors.mobile) {
            this.setState({
                disableSMS: true
            })
            let interval = setInterval(() => {

                if (this.state.clickNum === 0) {
                    this.setState({
                        disableSMS: false
                    });
                    clearInterval(interval)
                } else {
                    this.setState({
                        clickNum: this.state.clickNum - 1
                    });
                }
            }, 1000)
            try {
                const values = getFieldsValue(['mobile', 'prefix']);
                const { prefix, mobile } = values;
                await candoAPI.post(API_URL.AUTH.SMS_CODE, {
                    mobile: prefix === '61' ? '+' + prefix + mobile : mobile
                });
                message.success(t('singup.send_sms_success'), 4);
            } catch (e) {
                let error = ''
                if (e.response) {
                    error = e.response.data.mobile[0];
                }
                message.error(t('singup.send_sms_failed') + ':' + error, 4);
            }
        }
    }

    render() {
        const { getFieldDecorator, getFieldsValue } = this.props.form;
        const { t } = this.props;
        const { disableSMS, clickNum } = this.state;
        const prefixSelector = getFieldDecorator('prefix', {
            initialValue: '61',
        })(
            <Select style={{ width: 125 }}>
                <Select.Option value="61">{t('singup.australia')}+61</Select.Option>
                <Select.Option value="86">{t('singup.china')}+86</Select.Option>
            </Select>,
        );

        return (
            <div className='form-container'>
                <Row className='form-title'>
                    {t('auth.signup')}
                </Row>
                <Row>
                    <Form layout='vertical' onSubmit={this.handleSingUp}>
                        <Form.Item
                            label={t('singup.username')} >
                            {getFieldDecorator('username', {
                                initialValue:'',
                                rules: [{ required: true, message: t('singup.username_tip') }],
                            })(
                                <Input placeholder={t('singup.username_tip')} />
                            )}

                        </Form.Item>
                        <Form.Item label={t('singup.phone')}>
                            {getFieldDecorator('mobile', {
                                rules: [{
                                    validator: (rules, value, callback) => {
                                        if (!value) {
                                            callback(t('singup.phone_tip'))
                                        } else {
                                            const mobile = '+' + getFieldsValue(['prefix']).prefix + value;
                                            if (!validator.isMobilePhone(mobile, ['zh-CN', 'en-AU'])) {
                                                callback(t('singup.phone_tip_error'))
                                            }else {
                                                callback();
                                            }
                                        }
                                    }
                                }]
                            })(<Input addonBefore={prefixSelector} style={{ width: '100%' }} />)}
                        </Form.Item>
                        <Form.Item
                            label={t('singup.password')} >
                            {getFieldDecorator('password', {
                                rules: [{ required: true, message: t('singup.password_error') }],
                            })(
                                <Input placeholder={t('singup.password_tip')} type="password" />
                            )}
                        </Form.Item>
                        <Form.Item
                            label={t('singup.confirm_password')} >
                            {getFieldDecorator('confrom_password', {
                                rules: [
                                    {
                                        validator: (rules, value, callback) => {
                                            if (!value) {
                                                callback(t('singup.confirm_password_error'))
                                            } else {
                                                if (value !== getFieldsValue(['password']).password) {
                                                    callback(t('singup.confirm_password_not_same_error'))
                                                }else {
                                                    callback()
                                                }
                                            }

                                        }
                                    }],
                            })(
                                <Input placeholder={t('singup.confirm_password_tip')} type="password" />
                            )}
                        </Form.Item>
                        <Form.Item label={t('singup.verification_code')}>
                            <div className='flex-row'>
                                <div className='verification-code'>
                                    {getFieldDecorator('code', {
                                        rules: [{ required: true, message: t('singup.verification_code_error') }]
                                    })(<Input placeholder={t('singup.verification_code_tip')} />)}
                                </div>
                                <RenderSMSBtn clickNum={clickNum} getSMSCode={this.getSMSCode} disableSMS={disableSMS} t={t} />

                            </div>
                        </Form.Item>

                        <Form.Item>
                            <Button type="primary" htmlType="submit" className="login-form-button">
                                {t('auth.signup')}</Button>
                        </Form.Item>

                        <Form.Item>
                            <div className='remember-me'>
                                {getFieldDecorator('agree', {
                                    rules: [{
                                        validator: (rules, value, callback) => {
                                            if (!value) {
                                                callback(t('singup.cando_term_error'))
                                            }else {
                                                callback();
                                            }
                                        }
                                    }]
                                })(
                                    <Checkbox>{t('singup.term_agree')}{t('singup.cando_term')}</Checkbox>
                                )}
                                <div>
                                    <span>{t('singup.have_account')}</span>
                                    <Link to='/signin'>{t('singup.sign_in_now')}</Link>
                                </div>
                            </div>
                        </Form.Item>
                    </Form>
                </Row>
            </div>
        )
    }
}


export default compose(
    connect(null, actions),
    withTranslation(),
    Form.create({ name: 'signup_form' })
)(SignUp)
