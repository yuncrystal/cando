import React, { useState, useEffect, Fragment } from "react"
import { useTranslation } from "react-i18next"
import { message, Icon, Radio, Popconfirm } from "antd"
import { API_URL } from "../../api/apiUrl"
import requireAuth from "../../HOC/requireAuth"
import useResources from "../../hookResource/resourceDetail"
import useTrackResource from "../../hookResource/resourceTrack"
import moment from "moment"
import { CopyToClipboard } from "react-copy-to-clipboard"
import AddPackageModal from "../cando-components/addPackageModal"
import DeletePackageModal from "../cando-components/deletePackageModal"
import PhotoModal from "../cando-components/photoModal"
import candoAPI from "../../api/candoAPI"
import Loading from "../cando-error/loading"
import StripeCheckout from "react-stripe-checkout"
import qs from "qs"
import { Link } from "react-router-dom"
import { connect } from "react-redux"
import { compose } from "redux"
import { getUserInfo } from "../../actions/auth"

const OrderDetail = props => {
  const { match, account } = props
  const [modalVisibility, setModalVisibility] = useState(false)
  const [modalVisibilityDelete, setModalVisibilityDelete] = useState(false)
  const [modalVisibilityPhoto, setModalVisibilityPhoto] = useState(false)
  const [photoUrl, setPhotoUrl] = useState("")
  const [deletedItemId, setDeletedItemId] = useState(null)
  const [orderChanged, setOrderChanged] = useState(0)
  const [traceNum, setTraceNum] = useState(null)

  const orderId = match.params.id
  const { t } = useTranslation()
  let stripeBtn = React.createRef()

  const orderDetail = useResources(
    API_URL.ORDER.ORDER_DETAIL + `${orderId}/`,
    orderChanged,
    error => {
      message.error(t("orderDetail.get_order_error") + error, 8)
    }
  )

  useEffect(() => {
    props.getUserInfo()
    if (orderDetail) {
      if (orderDetail.status < 50) {
        if (orderDetail.rp_orderitems.length > 0) {
          setTraceNum(orderDetail.rp_orderitems[0].tracking_number)
        }
      } else {
        setTraceNum(orderDetail.tracking_number)
      }
    }
  }, [orderDetail])

  let trackDetail = useTrackResource(traceNum, error => {
    // message.error(t('orderDetail.get_track_failed') + error, 4)
  })

  const openModal = () => {
    setModalVisibility(true)
  }

  const closeModal = () => {
    setModalVisibility(false)
  }

  const openModalDelete = itemId => {
    setModalVisibilityDelete(true)
    setDeletedItemId(itemId)
  }

  const closeModalDelete = () => {
    setModalVisibilityDelete(false)
  }

  const openModalPhoto = imgUrl => {
    setPhotoUrl(imgUrl)
    setModalVisibilityPhoto(true)
  }

  const closeModalPhoto = () => {
    setModalVisibilityPhoto(false)
  }

  const changeRepackage = async e => {
    try {
      await candoAPI.post(
        API_URL.ORDER.CREATE_ORDER_REPACKAGE,
        qs.stringify({
          order_id: orderId,
          repackage: e.target.value
        })
      )
      setOrderChanged(orderChanged + 1)
      message.success(t("orderDetail.change_repackage_service_success"), 4)
    } catch (e) {
      message.error(t("orderDetail.change_repackage_service_failed"), 4)
    }
  }

  const addPackage = async data => {
    // console.log('add package data:', data)

    try {
      await candoAPI.post(API_URL.ORDER.ADD_PACKAGE, {
        order: orderId,
        orderitem_content: data.orderitem_content,
        tracking_number: data.tracking_number,
        weight: data.weight
      })
      closeModal()
      setOrderChanged(orderChanged + 1)
      message.success(t("add_package.add_package_success"), 4)
    } catch (error) {
      // const error = e.response.data
      // Object.keys(error).forEach(function (key) {
      //     console.log(key, error[key])
      // })
      message.error(t("add_package.add_package_failed"), 4)
    }
  }

  const deletePackage = async () => {
    try {
      await candoAPI.delete(API_URL.ORDER.ADD_PACKAGE + deletedItemId + "/")

      closeModalDelete()
      setOrderChanged(orderChanged + 1)
      message.success(t("delete_package.delete_success"), 4)
    } catch (e) {
      message.error(t("delete_package.delete_failed"), 4)
    }
  }

  const onToken = async token => {
    try {
      await candoAPI.post(
        API_URL.ORDER.CHECK_OUT,
        qs.stringify({
          orderid: orderId,
          token: token.id
        })
      )
      setOrderChanged(orderChanged + 1)
      message.success(t("orderDetail.pay_success"), 4)
    } catch (error) {
      message.error(t("orderDetail.pay_failed"), 4)
    }
  }

  const handlePackage = async () => {
    try {
      await candoAPI.post(
        API_URL.ORDER.READY_TO_PACKAGE,
        qs.stringify({
          order_id: orderId
        })
      )
      setOrderChanged(orderChanged + 1)
      message.success(t("orderDetail.ready_to_package_success"), 4)
    } catch (error) {
      message.error(t("orderDetail.ready_to_package_failed"), 4)
    }
  }

  const handlePay = async () => {
    try {
      await candoAPI.post(
        API_URL.ORDER.SKIP_CHARGE,
        qs.stringify({
          order_id: orderId
        })
      )
      message.success(t("orderDetail.pay_success"), 4)
      setOrderChanged(orderChanged + 1)
    } catch (error) {
      stripeBtn.current.onClick()
    }
  }

  const RenderTitle = ({ status }) => {
    return (
      <div className="status-context">
        {t("orderDetail.order_status")}：{t(`orderDetail.status.${status}`)}
      </div>
    )
  }

  const RenderNationalTrace = ({ status }) => {
    if (trackDetail) {
      const { com, nu, data } = trackDetail
      return (
        <div className="track-package-intl">
          <div className="title">
            {t("orderDetail.track_info")} {`(${com}:${nu})`}
          </div>
          <div className="flex-row track-row" style={{ background: "#f4f4f4" }}>
            <div className="time">{t("orderDetail.action_time")}</div>
            <div className="content">{t("orderDetail.action_record")}</div>
          </div>

          {data.map((site, index) => {
            return (
              <div key={index} className="flex-row track-row">
                <div className="time">{site.time}</div>
                <div className="content">{site.context}</div>
              </div>
            )
          })}
        </div>
      )
    } else {
      return (
        <div className="track-package-intl">
          <div className="title">{t("orderDetail.track_info")} </div>
          <div className="flex-row track-row" style={{ background: "#f4f4f4" }}>
            <div className="time">{traceNum}</div>
            <div className="content">{t("orderDetail.get_track_failed")}</div>
          </div>
        </div>
      )
    }
  }

  const RenderOperation = ({ status, price }) => {
    if (status >= 30) {
      if (status === 50) {
        return <div className="alert-text"> {t("orderDetail.send_alert")}</div>
      } else if (status === 100) {
        return (
          <div className="alert-text"> {t("orderDetail.complete_alert")}</div>
        )
      } else if (status === 110) {
        return (
          <div className="alert-text"> {t("orderDetail.complete_alert")}</div>
        )
      } else {
        return <div className="alert-text"> {t("orderDetail.pay_alert")}</div>
      }
    } else {
      if (status === 10) {
        return (
          <Popconfirm
            title={t("orderDetail.ready_to_package_message")}
            onConfirm={handlePackage}
            okText={t("orderDetail.ready_to_package_pop_up_yes")}
            cancelText={t("orderDetail.ready_to_package_pop_up_no")}
          >
            <div className="blue-btn margin-center">
              {t("orderDetail.ready_to_package")}
            </div>
          </Popconfirm>
        )
      } else if (status === 15) {
        return (
          <div className="pay-attention">{t("orderDetail.packing_now")}</div>
        )
      } else if (status === 20) {
        return (
          <div>
            <div onClick={handlePay} className="blue-btn margin-center">
              {t("orderDetail.pay_now")}
            </div>
            <div className="pay-attention">
              {t("orderDetail.before_pay_alert")}
            </div>
            <StripeCheckout
              style={{ display: "none" }}
              ref={stripeBtn}
              token={onToken}
              // stripeKey="pk_test_2yBYj84Q0BpKwKkJkVDOYgff"
              stripeKey="pk_live_vzydO6TkwUGyBbW4TQTYqNmO"
              name="cando"
              amount={price * 100} // cents
              currency="AUD"
            ></StripeCheckout>
          </div>
        )
      }
    }
  }

  const StatusIcon = ({ orderStatus, status }) => {
    return (
      <div className="order-status">
        {status < 10 ? (
          <Fragment>
            <div className="content">{t("orderDetail.status.0")}</div>
            <div className="icon">
              <i className="fas fa-check-circle"></i>
            </div>
          </Fragment>
        ) : (
          <Fragment>
            {orderStatus < status ? (
              <Fragment>
                <div className="content">
                  {t(`orderDetail.status.${status}`)}
                </div>
                <div className="stage">
                  {status > 50 ? (
                    <div>5</div>
                  ) : status < 20 ? (
                    <div>2</div>
                  ) : status < 30 ? (
                    <div>3</div>
                  ) : (
                    <div>4</div>
                  )}
                </div>
              </Fragment>
            ) : (
              <Fragment>
                <div className="content">
                  {t(`orderDetail.status.${status}`)}
                </div>
                <div className="icon">
                  <i className="fas fa-check-circle"></i>
                </div>
              </Fragment>
            )}
          </Fragment>
        )}
      </div>
    )
  }

  const RenderOrderStatus = ({ status }) => {
    return (
      <div className="order-status-progress">
        <div className="prgress-bar">
          <div className="prgress-line" />
          {status === 110 ? (
            <div className="prgress-status">
              <StatusIcon orderStatus={status} status={10} />
              <StatusIcon orderStatus={status} status={110} />
            </div>
          ) : (
            <div className="prgress-status">
              <StatusIcon orderStatus={status} status={10} />
              <StatusIcon orderStatus={status} status={15} />
              <StatusIcon orderStatus={status} status={20} />
              <StatusIcon orderStatus={status} status={30||40||50} />
              <StatusIcon orderStatus={status} status={100} />
            </div>
          )}
        </div>
      </div>
    )
  }

  if (orderDetail) {
    const {
      receiver,
      address,
      phone,
      create_date,
      shipping_company_address,
      shipping_company_phone,
      rp_orderitems,
      price,
      status
    } = orderDetail
    const { box_volume_weight, box_weight, tracking_number, repackage } = orderDetail
    return (
      <div className="order-detail-container">
        <div className="return-link">
          <Link to="/trace_manage">{t("button.return_list")}</Link>
        </div>
        <RenderOrderStatus status={status} />
        <div className="order-detail">
          <div className="left-content">
            <div className="title">{t("orderDetail.order_detail")}：</div>
            <div className="detail">
              <div>
                {t("orderDetail.receiver")}：{`${receiver}, ${phone}`}
              </div>
              <div>
                {t("orderDetail.receiver_address")}：{address}
              </div>
              <div>
                {t("orderDetail.order_number")}：{orderId}
              </div>
              <div>
                {t("orderDetail.order_create_time")}：
                {moment(create_date).format("YYYY-MM-DD HH:mm:ss")}
              </div>
              <div>
                {t("orderDetail.order_fee")}：{t("aud")}
                {price}
              </div>
              <div>
                {t("orderDetail.package_weight")}：{box_weight} g
              </div>
              <div>
                {t("text.volume_weight")}：{box_volume_weight} g
              </div>
              <div>
                {t("newOrder.choose_repackage")}：
                {repackage
                  ? t("newOrder.repackage")
                  : t("newOrder.remain_package")}
              </div>
              <div
                onClick={() => {
                  setTraceNum(tracking_number)
                  window.scrollTo({
                    top: document.body.scrollHeight,
                    behavior: "smooth"
                  })
                }}
              >
                {t("orderDetail.tracking_number")}：
                <span className="item-trace-num">{tracking_number}</span>
              </div>
            </div>
          </div>

          <div className="right-content">
            <div className="order-status">
              <Icon type="check-circle" className="status-icon" />
              <RenderTitle status={status} />
            </div>
          </div>
        </div>

        <div className="order-transport">
          <div className="title">{t("orderDetail.transport_info")}</div>
          <div className="relay-address">
            <div className="left">{t("orderDetail.relay_address")}</div>
            <div className="right">
              <div>{`${
                account ? account.username : ""
              }, ${shipping_company_phone}, ${shipping_company_address}`}</div>
              <CopyToClipboard
                text={`${receiver}, ${shipping_company_phone}, ${shipping_company_address}`}
                onCopy={(text, result) => {
                  if (!result) {
                    message.error(t("orderDetail.coyp_failed"), 4)
                  } else {
                    message.success(t("orderDetail.copy_success"), 4)
                  }
                }}
              >
                <div className="blue-btn">{t("orderDetail.copy_text")}</div>
              </CopyToClipboard>
            </div>
          </div>
          <div className="delivery-address">
            <div className="left">{t("orderDetail.delivery_address")}</div>
            <div className="right">
              <div>{`${receiver}, ${phone}, ${address}`}</div>
              <CopyToClipboard
                text={`${receiver}, ${phone}, ${address}`}
                onCopy={(text, result) => {
                  if (!result) {
                    message.error(t("orderDetail.coyp_failed"), 4)
                  } else {
                    message.success(t("orderDetail.copy_success"), 4)
                  }
                }}
              >
                <div className="blue-btn">{t("orderDetail.copy_text")}</div>
              </CopyToClipboard>
            </div>
          </div>
          {status === 10 ? (
            <div className="package-service">
              <div className="left">{t("newOrder.choose_repackage")}</div>
              <div className="right">
                <Radio.Group
                  defaultValue={repackage ? 1 : 0}
                  onChange={changeRepackage}
                >
                  <Radio value={1}>{t("newOrder.repackage")}</Radio>
                  <Radio value={0}>{t("newOrder.remain_package")}</Radio>
                </Radio.Group>
              </div>
            </div>
          ) : null}
        </div>
        <div className="order-item-table-wrap">
          {status === 110 ? null : (
            <table className="order-items-table">
              <thead>
                <tr>
                  <th className="item-name"> {t("orderList.packages")}</th>
                  <th className="item-status">
                    {" "}
                    {t("orderList.packages_status")}
                  </th>
                  {status === 20 || status === 40 || status === 50 ? null : (
                    <th className="item-delete"></th>
                  )}

                  <th className="item-operation">
                    {" "}
                    {t("orderList.operation")}
                  </th>
                </tr>
              </thead>
              <tbody>
                {rp_orderitems.map((item, index) => {
                  const {
                    id,
                    is_received,
                    tracking_number,
                    create_date,
                    image,
                    orderitem_content,
                    weight,
                    volume_weight
                  } = item
                  return (
                    <tr key={item.tracking_number}>
                      <td>
                        <div className="item-info">
                          <div className="item-img">
                            {is_received && image ? (
                              <img
                                src={image}
                                alt="item-img"
                                onClick={() => openModalPhoto(image)}
                              />
                            ) : (
                              <span className="img-icon">
                                <i className="fas fa-images"></i>
                              </span>
                            )}
                          </div>
                          <div className="item-details">
                            <div>
                              {t("orderList.packages")}：{orderitem_content}
                            </div>
                            <div>
                              {t("orderList.weight")}：{weight} g
                            </div>
                            <div>
                              {t("text.volume_weight")}：{volume_weight} g
                            </div>
                            <div
                              onClick={() => {
                                setTraceNum(tracking_number)
                                window.scrollTo({
                                  top: document.body.scrollHeight,
                                  behavior: "smooth"
                                })
                              }}
                            >
                              {t("orderList.package_num")}：
                              <span className="item-trace-num">
                                {tracking_number}
                              </span>
                            </div>
                            <div>
                              {t("orderList.package_create_time")}：
                              {moment(create_date).format(
                                "YYYY-MM-DD HH:mm:ss"
                              )}
                            </div>
                          </div>
                        </div>
                      </td>
                      <td className="item-status">
                        {is_received
                          ? t("orderDetail.platform_received")
                          : t("orderDetail.not_arrive")}
                      </td>
                      {status === 20 ||
                      status === 40 ||
                      status === 50 ? null : (
                        <td className="item-delete">
                          {is_received ? (
                            <i className="fas fa-trash-alt delete-cancel"></i>
                          ) : (
                            <i
                              className="fas fa-trash-alt delete-item"
                              onClick={() => {
                                openModalDelete(id)
                              }}
                            ></i>
                          )}
                        </td>
                      )}

                      {index === 0 ? (
                        <td
                          className="item-operation"
                          rowSpan={`${rp_orderitems.length}`}
                        >
                          {<RenderOperation status={status} price={price} />}
                        </td>
                      ) : null}
                    </tr>
                  )
                })}
                {status >= 20 ? (
                  <tr className="pay-info">
                    <td colSpan="4">
                      <div className="pay-info-layout">
                        {/* <div className='pay-detail-row'>
                                                            <div>{t('orderDetail.package_standards')}:</div>
                                                            <div>{`${length}*${width}*${height}(CM)`}</div>
                                                        </div> */}
                        <div className="pay-detail-row">
                          <div>{t("orderDetail.package_weight")}:</div>
                          <div>{box_weight}(g)</div>
                        </div>
                        <div className="pay-detail-row">
                          <div>{t("text.volume_weight")}:</div>
                          <div>{box_volume_weight}(g)</div>
                        </div>
                        <div className="pay-detail-row">
                          <div>{t("orderDetail.package_fee")}:</div>
                          <div>
                            {t("aud")}
                            {price}
                          </div>
                        </div>
                        <div className="pay-detail-row">
                          <div>{t("orderDetail.package_total_fee")}:</div>
                          <div>
                            {t("aud")}
                            {price}
                          </div>
                        </div>
                        <div className="pay-detail-row">
                          <div>{t("orderDetail.client_pay")}:</div>
                          <div>
                            {t("aud")}
                            {price}
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                ) : (
                  <tr className="add-package">
                    <td colSpan="4">
                      <div
                        style={{ margin: "auto" }}
                        className="blue-btn"
                        onClick={() => openModal()}
                      >
                        {t("orderDetail.add_package")}
                      </div>
                      {/* <span className='add-package-icon' onClick={() => openModal()}>
                                                        <i className="fas fa-plus-circle"></i>{t('orderDetail.add_package')}
                                                    </span> */}
                    </td>
                  </tr>
                )}
              </tbody>
            </table>
          )}
        </div>
        {status === 110 ? null : <RenderNationalTrace status={status} />}

        <AddPackageModal
          closeModal={closeModal}
          modalVisibility={modalVisibility}
          addPackage={addPackage}
        />
        <DeletePackageModal
          closeModal={closeModalDelete}
          modalVisibility={modalVisibilityDelete}
          deletePackage={deletePackage}
          title={t("delete_package.confirm_delete")}
          content={t("delete_package.delete_alert")}
        />
        <PhotoModal
          closeModal={closeModalPhoto}
          modalVisibility={modalVisibilityPhoto}
          img={photoUrl}
        />
      </div>
    )
  } else {
    return (
      <div style={{ height: "100%" }}>
        <Loading />
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    account: state.auth.account
  }
}
export default compose(
  requireAuth,
  connect(mapStateToProps, { getUserInfo })
)(OrderDetail)
