
import React from 'react'
import { useTranslation } from 'react-i18next'
import { API_URL } from '../../api/apiUrl'
import useResources from '../../hookResource/resourceList'
import OrderTable from '../cando-components/orderTable'
import { message } from 'antd'
import Loading from '../cando-error/loading'
import EmptyList from '../cando-components/emptyList'
import requireAuth from '../../HOC/requireAuth'



const Completed = (props) => {
    const { t } = useTranslation()
    let allOrderList
    allOrderList = useResources(API_URL.ORDER.COMPLETED_ORDER, () => {
        message.error(t('orderList.get_all_order_error'), 5)
    })

    return (
        <div className='order-list-container'>
            <div className='order-list'>
                {
                    allOrderList
                        ?
                        allOrderList.length === 0
                            ? <EmptyList content={t('orderList.no_complete_orders')} />
                            : allOrderList.map((order) => {
                                return <OrderTable order={order} key={order.id} />
                            })
                        :
                        <Loading />
                }

            </div>

        </div>
    )
}


export default requireAuth(Completed)