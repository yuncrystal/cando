
import React,{useState} from 'react';
import { useTranslation } from 'react-i18next';
import { API_URL } from '../../api/apiUrl';
import PropTypes from 'prop-types';
import useResources from '../../hookResource/resourceList';
import OrderTable from '../cando-components/orderTable';
import { message } from 'antd';
import Loading from '../cando-error/loading'
import EmptyList from '../cando-components/emptyList';
import requireAuth from '../../HOC/requireAuth';
import candoAPI from '../../api/candoAPI';


const OrderList = (props) => {
    const { t } = useTranslation();
    const [update,setUpdate] = useState(0)
    const { resourcesUrl, emptyContent, emptyLink, emptyLlinkContent,updateOrderList } = props;
    let allOrderList;
    allOrderList = useResources(resourcesUrl, () => {
        message.error(t('orderList.get_all_order_error'), 5);
    },update);

    const updateList = ()=>{
        setUpdate(update+1)
        updateOrderList();
    }

    const handleConfirm = async (id) => {
        try {
            await candoAPI.get(API_URL.ORDER.COMPLETE + '?id=' + id);
            setUpdate(update + 1)
            updateOrderList();
            message.success(t('orderList.complete_order_success'), 4);
        } catch (e) {
            message.error(t('orderList.complete_order_failed'), 4);
        }
    }

    return (
        <div className='order-list-container'>
            <div className='order-list'>
                {
                    allOrderList
                        ?
                        allOrderList.length === 0
                            ? <EmptyList content={t(emptyContent)} link={emptyLink} linkContent={t(emptyLlinkContent)} />
                            : allOrderList.map((order) => {
                                return <OrderTable order={order} key={order.id} completeOrder={handleConfirm} updateList={updateList}/>
                            })
                        :
                        <Loading />
                }

            </div>

        </div>
    )
}
OrderList.propTypes = {
    resourcesUrl: PropTypes.string.isRequired,
    emptyContent: PropTypes.string.isRequired,
    emptyLink: PropTypes.string,
    emptyLlinkContent: PropTypes.string,
    updateOrderList:PropTypes.func.isRequired
};

export default requireAuth(OrderList)
