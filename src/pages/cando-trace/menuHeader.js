
import React from 'react';
import { NavLink} from "react-router-dom";
import menuConfig from './menuConfig';
import { useTranslation } from 'react-i18next';

const MenuHeader = () => {
    const { t } = useTranslation();
    return (
        <div className='menu-header'>
            {
                menuConfig[0].content.map((menu) => {
                    return (
                        <NavLink exact className='menu-item' to={`/trace_manage${menu.url}`} key={menu.key} activeClassName="menu-selected">
                            {t(menu.item)}
                        </NavLink>
                    )
                })
            }
        </div>
    )
}

export default MenuHeader;