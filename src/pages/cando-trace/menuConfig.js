const menuConfig = [
    {
        key: 'subMenu1',
        title: 'traceMenu.subMenu1.title',
        icon:'fa-clipboard',
        content: [
            
            {
                key: '1',
                item: 'traceMenu.subMenu1.content.assignedOrder',
                url: '',
                status:'10'
            },
            {
                key: '2',
                item: 'traceMenu.subMenu1.content.packingOrder',
                url: '/packing',
                status:'15'
            },
            {
                key: '3',
                item: 'traceMenu.subMenu1.content.waitPaymentOrder',
                url: '/wait_payment',
                status:'20'
            },
            {
                key: '4',
                item: 'traceMenu.subMenu1.content.inTransitOrder',
                url: '/in_transit',
                status:'in_transit'
            },
            {
                key: '5',
                item: 'traceMenu.subMenu1.content.completedOrder',
                url: '/finished',
                status:'100'
            },
            {
                key: '6',
                item: 'traceMenu.subMenu1.content.cancelledOrder',
                url: '/canceled',
                status:'110'
            },
            {
                key: '7',
                item: 'traceMenu.subMenu1.content.allOrder',
                url: '/all',
                status:'all'
            }
        ]
    }
]

export default menuConfig;
