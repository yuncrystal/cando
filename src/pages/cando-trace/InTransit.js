
import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { API_URL } from '../../api/apiUrl'
import useResources from '../../hookResource/resourceList'
import OrderTable from '../cando-components/orderTable'
import { message } from 'antd'
import Loading from '../cando-error/loading'
import EmptyList from '../cando-components/emptyList'
import candoAPI from '../../api/candoAPI'
import requireAuth from '../../HOC/requireAuth'


const InTransit = (props) => {
    const { t } = useTranslation()
    const [orderChanged, setOrderChanged] = useState(1)
    let allOrderList
    allOrderList = useResources(API_URL.ORDER.IN_TRANSIT_ORDER, () => {
        message.error(t('orderList.get_all_order_error'), 4)
    }, orderChanged)


    const handleConfirm = async (id) => {
        try {
            await candoAPI.get(API_URL.ORDER.COMPLETE + '?id=' + id)
            setOrderChanged(orderChanged + 1)
            message.success(t('orderList.complete_order_success'), 4)
        } catch (e) {
            message.error(t('orderList.complete_order_failed'), 4)
        }
    }

    return (
        <div className='order-list-container'>

            <div className='order-list'>
                {
                    allOrderList
                        ?
                        allOrderList.length === 0
                            ? <EmptyList content={t('orderList.no_waiting_receive_orders')} />
                            : allOrderList.map((order) => {
                                return <OrderTable order={order} key={order.id} completeOrder={handleConfirm} />
                            })
                        :
                        <Loading />
                }

            </div>

        </div>
    )
}

export default requireAuth(InTransit)
