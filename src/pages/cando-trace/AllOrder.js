
import React from 'react'
import { useTranslation } from 'react-i18next'
import requireAuth from '../../HOC/requireAuth'
import { API_URL } from '../../api/apiUrl'
import useResources from '../../hookResource/resourceList'
import OrderTable from '../cando-components/orderTable'
import { message } from 'antd'
import Loading from '../cando-error/loading'
import EmptyList from '../cando-components/emptyList'
// import MenuHeader from './menuHeader'

const AllOrder = () => {
    const { t } = useTranslation()
    let allOrderList
    allOrderList = useResources(API_URL.ORDER.ALL_ORDER, () => {
        message.error(t('orderList.get_all_order_error'), 5)
    })
    return (
        <div className='order-list-container'>
            <div className='order-list'>
                {
                    allOrderList
                        ?
                        allOrderList.length === 0
                            ? <EmptyList content={t('orderList.no_orders')} linkContent={t('orderList.add_order')} link={'/place_order'} />
                            : allOrderList.map((order) => {
                                return <OrderTable order={order} key={order.id} />
                            })

                        :
                        <Loading />
                }

            </div>

        </div>
    )
}


export default requireAuth(AllOrder)

