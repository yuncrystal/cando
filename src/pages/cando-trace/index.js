import React, { useState, useEffect } from "react"
import { connect } from "react-redux"
import requireAuth from "../../HOC/requireAuth"
import Menu from "../cando-components/menu"
import { Route, Switch } from "react-router-dom"
import menuConfig from "./menuConfig"
import OrderList from "./OrderList"
import NoMatch from "../cando-error/error"
import { API_URL } from "../../api/apiUrl"

const TraceAndManage = props => {
  // const [menuKey, setMenuKey] = useState(null)

  const [updateList, setUpdateList] = useState(0)

  let menuKey
  const { location } = props
  let currentUrl = location.pathname
  if (currentUrl.includes("all")) {
    menuKey = "7"
  } else if (currentUrl.includes("packing")) {
    menuKey = "2"
  } else if (currentUrl.includes("wait_payment")) {
    menuKey = "3"
  } else if (currentUrl.includes("in_transit")) {
    menuKey = "4"
  } else if (currentUrl.includes("finished")) {
    menuKey = "5"
  } else if (currentUrl.includes("canceled")) {
    menuKey = "6"
  } else {
    menuKey = "1"
  }

  const { match, history } = props

  const updateOrderList = () => {
    setUpdateList(updateList + 1)
  }

  return (
    <div className="main-layout">
      <Menu
        menuConfig={menuConfig}
        menuTitle={"traceMenu.title"}
        defaultOpenKeys={["subMenu1"]}
        defaultSelectedKeys={[menuKey]}
        match={match}
        selectedKeys={["4"]}
        history={history}
        updateList={updateList}
      />

      <Switch>
        <Route
          exact
          path={`${match.path}/`}
          component={() => (
            <OrderList
              updateOrderList={updateOrderList}
              resourcesUrl={API_URL.ORDER.ASSIGNED_ORDER}
              emptyContent={"orderList.no_assigned_orders"}
              emptyLink={"/place_order"}
              emptyLlinkContent={"orderList.add_order_now"}
            />
          )}
        />
        <Route
          exact
          path={`${match.path}/packing`}
          component={() => (
            <OrderList
              updateOrderList={updateOrderList}
              resourcesUrl={API_URL.ORDER.PACKING_ORDER}
              emptyContent={"orderList.no_waiting_ship_orders"}
            />
          )}
        />
        <Route
          exact
          path={`${match.path}/wait_payment`}
          component={() => (
            <OrderList
              updateOrderList={updateOrderList}
              resourcesUrl={API_URL.ORDER.WAITING_PAYMENT}
              emptyContent={"orderList.no_waiting_receive_orders"}
            />
          )}
        />
        <Route
          exact
          path={`${match.path}/in_transit`}
          component={() => (
            <OrderList
              updateOrderList={updateOrderList}
              resourcesUrl={API_URL.ORDER.IN_TRANSIT_ORDER}
              emptyContent={"orderList.no_waiting_receive_orders"}
            />
          )}
        />
        <Route
          exact
          path={`${match.path}/finished`}
          component={() => (
            <OrderList
              updateOrderList={updateOrderList}
              resourcesUrl={API_URL.ORDER.COMPLETED_ORDER}
              emptyContent={"orderList.no_complete_orders"}
            />
          )}
        />
        <Route
          exact
          path={`${match.path}/canceled`}
          component={() => (
            <OrderList
              updateOrderList={updateOrderList}
              resourcesUrl={API_URL.ORDER.CANCELED_ORDER}
              emptyContent={"orderList.no_canceled_orders"}
            />
          )}
        />
        <Route
          exact
          path={`${match.path}/all`}
          component={() => (
            <OrderList
              updateOrderList={updateOrderList}
              resourcesUrl={API_URL.ORDER.ALL_ORDER}
              emptyContent={"orderList.no_orders"}
              emptyLink={"/place_order"}
              emptyLlinkContent={"orderList.add_order_now"}
            />
          )}
        />
        <Route component={NoMatch} />
      </Switch>
    </div>
  )
}

export default connect()(requireAuth(TraceAndManage))
