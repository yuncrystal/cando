
import React from 'react';
import { withTranslation } from 'react-i18next';
import destination from './destination.png'
import freightType from './freight-type.png'
import banner from './delivery.jpg'
import { Link } from 'react-router-dom'



const configDeatils = [
    {
        key: 1,
        background: 'detail1-img',
        title: 'detail1.title',
        content: 'detail1.centent'
    },
    {
        key: 2,
        background: 'detail2-img',
        title: 'detail2.title',
        content: 'detail2.centent'
    },
    {
        key: 3,
        background: 'detail3-img',
        title: 'detail3.title',
        content: 'detail3.centent'
    },
    {
        key: 4,
        background: 'detail4-img',
        title: 'detail4.title',
        content: 'detail4.centent'
    },
]

const Homepage = ({ t }) => {

    return (
        <div className='home-container'>
            <div className='banner'>
                <img src={banner} alt='cando-banner'/>
            </div>
            <div className='advantage-container flex-column center-h'>
                <div className='title'>
                    {t('home.advantage.title')}
                </div>
                <div className='advantages flex-row'>
                    <div className='advantage flex-column center-all'>
                        <div className='img adv1-img'></div>
                        <div className='title'>{t('home.advantage.adv1.title')}</div>
                        <div className='content'>{t('home.advantage.adv1.content')}</div>
                        <br/><Link to='/homeSampleOrderIntroduce'><strong>了解更多</strong></Link>
                    </div>
                    <div className='advantage flex-column center-all'>
                        <div className='img adv2-img'></div>
                        <div className='title'>{t('home.advantage.adv2.title')}</div>
                        <div className='content'>{t('home.advantage.adv2.content')}</div>
                        <br/><Link to='/homeIntroduceCurrentExpress'><strong>了解更多</strong></Link>
                    </div>
                    <div className='advantage flex-column center-all'>
                        <div className='img adv3-img'></div>
                        <div className='title'>{t('home.advantage.adv3.title')}</div>
                        <div className='content'>{t('home.advantage.adv3.content')}</div>
                        <br/><Link to='/homeIntroduceRemoteRegion'><strong>了解更多</strong></Link>
                    </div>
                </div>
            </div>
            <div className='new-user-container flex-column center-all'>
                <div className='headers'>
                    <div className='title'>
                        {t('home.beginners.title')}
                    </div>
                    <div className='content' dangerouslySetInnerHTML={{ __html: t('home.beginners.content') }} />
                </div>
                <div className='details flex-row center-all'>
                    {
                        configDeatils.map((detail) => {
                            return (
                                <div className='detail flex-column center-v' key={detail.key}>
                                    <div className={`detail-img ${detail.background}`}></div>
                                    <div className='title'>{t(`home.beginners.${detail.title}`)}</div>
                                    <div className='content' dangerouslySetInnerHTML={{ __html: t(`home.beginners.${detail.content}`) }} />
                                </div>
                            )
                        })
                    }
                </div>

            </div>

            <div className='pay-standar-container flex-column center-all'>
                <div className='headers'>
                    <div className='title'>
                        {t('home.charge.title')}
                    </div>
                    <div className='content' dangerouslySetInnerHTML={{ __html: t('home.charge.content') }} />
                </div>
                <div className='type-container flex-row'>
                    <div className='type flex-column center-all'>
                        <div className='img'>
                            <img src={destination} alt='destination'/>
                        </div>
                        <div className='title'>
                            {t('home.charge.type1.title')}
                        </div>
                        <div className='content'>
                            {t('home.charge.type1.content')}
                        </div>
                        <div className='button bk-orange'>
                        <Link to='/homeIntroduceExpressType' style={{color: 'white'}}>{t('home.charge.type1.btn')}</Link> 
                        </div>
                    </div>
                    <div className='type flex-column center-all'>
                        <div className='img'>
                            <img src={freightType} alt='type'/>
                        </div>
                        <div className='title'>
                            {t('home.charge.type2.title')}
                        </div>
                        <div className='content'>
                            {t('home.charge.type2.content')}
                        </div>
                        <div className='button bk-blue'>
                        <Link to='/homeIntroduceDestination' style={{color: 'white'}}> {t('home.charge.type2.btn')}</Link>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    )
}

export default withTranslation()(Homepage);
