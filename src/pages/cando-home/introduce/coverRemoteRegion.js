

import React from 'react';

const introduceRemoteRegion = () => {
  return (
    <div className='new-order-container'>
      <div className='service'>
       <h2><strong>覆盖偏远地区</strong></h2><br/>
    <div><strong>不论您在什么地方采购的物品我们都可以帮您安全放心的运到您所要求的地点</strong>
    </div>
    </div>
</div>
  )
}

export default introduceRemoteRegion;