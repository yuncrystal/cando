import React from 'react';
import { Link } from 'react-router-dom'

const introduceSampleOrder = () => {

  const formIntroduceText = {
    content: {
      width: '615px',
      color: 'red'
  }
};

  return (
    <div className='new-order-container'>
      <div className='service'>
      <h2><strong>简单快速下单</strong></h2><br/><br/>
    <strong>新手上路:</strong>
    1.首先首页右上角点击 <Link to='/signup'>注册</Link> ，注册完输入登录名和密码就可以登录了。<br/><br/>
    2.登录完点击<Link to='/place_order'>立刻下单</Link> ，选择您要选择的运输服务类型。编辑您在<Link to='/my-center/manage-address'>澳洲的收货地址</Link><br/><br/>
    3.确认打包服务，点击<Link to='/terms'>我已理解并同意遵守服务条款</Link>，<strong>确认下单</strong>。<br/><br/>
    4.系统会显示出您的订单的订单信息，打开订单操作一栏中的<Link to='/trace_manage'>订单详情</Link>。系统会显示出一栏配送信息，配送信息中的一条您的中转地址，<span  style={{color: 'red'}} >这个很重要</span>，这个中转地址实际上是您在中国的收货地址，所以一定要复制这个信息填入您购买的物品在中国的收货地址。<br/><br/>
    5.将您的中国的运单号码添加进系统。操作是，登录系统后点击追踪和管理，在平台收货中，点击添加运单。至此您的下单操作已经成功了。<br/><br/>
    6.当您在平台收货中已经看到您所有货物的照片时，您可以付款打包了。
    </div>
</div>
  )

}

export default introduceSampleOrder;