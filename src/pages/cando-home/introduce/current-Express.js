
import React from 'react';
import { Link } from 'react-router-dom'

const introduceCurrentExpress = () => {
  return (
    <div className='new-order-container'>
      <div className='service'>
       <h2><strong>实时物流查询</strong></h2><br/>
       <h3><strong>我们的平台可以帮助您随时追踪您的货物在中国和澳洲的物流情况</strong></h3> <br/>
       <p>1.登录进Cando的网站，点击<Link to='/trace_manage'>追踪和管理</Link>的平台上，打开货物的订单详情您可以看到您订单中所有货物的的状况。</p><br/>
  <p>
    2.点击您货物的运单号可以追踪每个货物的运输情况。
  </p>
  </div>
</div>
  )
}

export default introduceCurrentExpress;
