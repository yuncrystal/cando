
import React from 'react';

const introduceExpressType = () => {
  return (
    <div className='new-order-container'>
      <div className='service'>
       <h2><strong>货运类型</strong></h2>
  <h1>我们公司的航空业务的承运范围是：</h1> 
  <p><strong>普通货物：</strong>印刷品（包括文件、书籍、广告品名片易拉宝广告牌等）、衣物（包括衣服鞋袜帽、毛巾浴巾围巾、行李箱、手提包、背包等）、不带电容和电池玩具、电子数码产品（不带磁的手机壳Ipad套、数据线）、化妆品和工具（假发假睫毛、睫毛工具、美容美发工具等）、文具（铅笔毛笔、本子、书包等。注意墨水笔里不能有水）、塑料铁质家具及非木制家具、五金工具机器（不含电池和电容非通电类）、家居物品（窗帘、清洁工具、厨房用品等）、运动器材（非电动）、建筑材料（非粉末状）、汽车用品零部件等等。</p> 
  <p><strong>普通货物：</strong>可以选择我们所有的服务，但是Easy Go 是最优惠的一种服务。一般来说需要7-14个工作日。Fast Go 是一种快速的运输服务，一般只要7个左右的工作日。<br /><br /> 普通货物的收费标准： Easy Go每半公斤4澳元。但是需要自提。<br /> Fast Go首重半公斤10澳元，续重6澳元。</p> 
  <p><strong>敏感货物：</strong>带电池、带磁、带电容的电器和玩具和电子产品；带磁手机壳；通电用家电和机器；打印机；消磁产品；拳头大小的电机。此类货物通关较慢，海关可能没收或退回，风险客户承担，空运快递费不退，可能产生退件费。批量敏感货物请与空运客服联系。</p> 
  <p><strong>敏感货物：</strong>只能选择Super Go,这是一种针对敏感货物的服务，一般是3-7个工作日 Super Go 的收费标准：是首重半公斤30澳元，续重6澳元</p> 
  <p><strong>特殊货物：</strong>非液体类食品零食干货、少量中成药和非处方药、非液体化妆品、墨盒、手机、电脑等。此类货物不能及时发货时效较慢，海关可能没收或退回，风险客户承担，空运快递费不退，可能产生退件费。</p> 
  <p><strong>特殊货物：</strong>只能选择Snack Go, 这是一种针对食品类的货运服务。 Snack Go的收费标准：是首重半公斤20澳元，续重6澳元。</p> 
  <p><strong>航空禁运物品：</strong>易燃易爆物品（包括油类汽油油漆指甲油等、压缩气体、罐装气体、烟花爆竹、纯电池、易燃化学品、打火机等）液体物品（如酒精酒类、胶水香水天拿水指甲延长胶等）、粉末状物品（包括药粉、毒品、化学粉末等）、化学物品、处方药品、动物制品包括尸骨标本、植物及种子、珠宝货币等贵重物品、烟草香烟、武器包括刀具和仿真枪、反动淫秽印刷品和音像制品、盗用品牌的山寨货以及中国和澳大利亚海关规定的其他违禁物品。因为客户托运以上物品造成的经济损失和法律责任都由客户承担。</p> 
  <p><strong>免责声明：</strong>我们只负责组织运输，质量和保修均与我们无关。</p> 
  <p><strong>注意：</strong>1.所有的重量指的是包含包装箱等包装材料来算的。价格也会根椐不同时期的活动会有少许变化（请注意公司的活动广告）<br /> 2.所有重量的结算是按照货物的实际重量和实际的体积重量两者取大值来结算运费的. （实际体积重量的计算公式是：货物的长*宽*高/5000）</p>   
 </div>
    </div>
  )
}
export default introduceExpressType;