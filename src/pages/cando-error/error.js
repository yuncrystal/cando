
import React from 'react';
import { useTranslation } from 'react-i18next';

const Error = () => {
    const { t } = useTranslation();
    return (
        <div className='error-container'>
            <div className='title'> 404</div>
            <div className='status'>
                {t('error.not_find')}
            </div>
        </div>
    )
}

export default Error;