import { useState, useEffect } from 'react';
import candoAPI from '../api/candoAPI';


const useResources = (apiURL, change, errorCallBack) => {
    const [resources, setResources] = useState([]);
    useEffect(() => {
        let unmounted = false;
        const getResource = async () => {
            try {
                const response = await candoAPI.get(apiURL);
                if (!unmounted) setResources(response.data);
            } catch (err) {
                errorCallBack(err)
            }
        }
        getResource()
        return () => {
            unmounted = true;
        };
    }, [change]);
    return resources;
}

export default useResources;


