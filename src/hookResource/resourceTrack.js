import { useState, useEffect } from 'react';
import candoAPI from '../api/candoAPI';
import { API_URL } from '../api/apiUrl';

const useResources = (trackNum, errorCallBack) => {
    const [resources, setResources] = useState(null);
    useEffect(() => {
        let unmounted = false;
        const getResource = async () => {
            if (trackNum) {
                try {
                    const response = await candoAPI.get(`${API_URL.ORDER.TRACE}?tracking_num=${trackNum}`);
                    if (!unmounted) setResources(response.data);
                } catch (err) {
                    if (!unmounted) setResources(null);
                    errorCallBack(err)
                }
            }
        }
        getResource();
        return () => {
            unmounted = true;
        };
    }, [trackNum]);
    return resources;
}

export default useResources;


