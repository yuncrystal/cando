export const API_URL = {
  AUTH: {
    CHECK_TOKEN: '/auth/jwt/verify/',
    SIGNIN: '/auth/jwt/create/',
    USER_INFO: '/auth/me/',
    SMS_CODE: '/register_sms_code/',
    FORGET_SMS_CODE: '/forget_password_sms_code/',
    USER_SMS_CODE: '/show_username/',
    REGISTER: '/account/',
    CHANGE_PASSWORD: '/auth/password/',
    RESET_PASSWORD: '/password_reset/'
  },
  ADDRESS: {
    CREATE_ADDRESS: '/customer_express_address/',
    All_ADDRESS: '/customer_express_address/'
  },
  SERVICE: {
    All_SERVICE: '/express_service/?ordering=seq'
  },
  ORDER: {
    CREATE_ORDER: '/customer_order/',
    CREATE_ORDER_REPACKAGE: '/customer_order_repackage/',
    ALL_ORDER:
      '/customer_order_with_orderitem/?max_status=110&min_status=0&ordering=-create_date',
    ASSIGNED_ORDER:
      '/customer_order_with_orderitem/?ordering=-create_date&status=10',
    PACKING_ORDER:
      '/customer_order_with_orderitem/?ordering=-create_date&status=15',
    WAITING_PAYMENT:
      '/customer_order_with_orderitem/?ordering=-create_date&status=20',
    IN_TRANSIT_ORDER:
      '/customer_order_with_orderitem/?ordering=-create_date&min_status=30&max_status=99',
    COMPLETED_ORDER:
      '/customer_order_with_orderitem/?ordering=-create_date&status=100',
    CANCELED_ORDER:
      '/customer_order_with_orderitem/?ordering=-create_date&status=110',
    // WAITING_SHIP_ORDER:
    //   '/customer_order_with_orderitem/?ordering=-create_date&min_status=20&max_status=40',
    ORDER_DETAIL: '/customer_order_with_orderitem/',
    ADD_PACKAGE: '/customer_predict_orderitem/',
    CHECK_PACKAGE_RECEIVED: '/customer_check_received_status/',
    CHECK_OUT: '/customer_order_charge2/',
    SKIP_CHARGE: '/customer_skip_charge2/',
    TRACE: '/tracking_info/',
    DELETE: '/customer_order_status_to_cancalled/',
    COMPLETE: '/customer_order_status_to_completed/',
    READY_TO_PACKAGE: '/customer_order_status_to_r2p/'
  }
}
