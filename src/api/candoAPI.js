import axios from 'axios';
import { TOKEN } from '../types/common';

if(localStorage.getItem(TOKEN)){
    axios.defaults.headers.common['Authorization'] = 'jwt '+localStorage.getItem(TOKEN);
}

const cando_api = axios.create({
    // baseURL: 'http://otto-shipping-dev.ap-southeast-2.elasticbeanstalk.com/',
    // baseURL: 'http://live-ottoshippingbackend-dev.ap-southeast-2.elasticbeanstalk.com/',
    baseURL: 'https://api.candoshippingapi.net/',
    // baseURL: 'http://127.0.0.1:8000/',
    
    // timeout: 10000,
})

export default cando_api;