import React, { Component } from 'react';
import { connect } from 'react-redux';
import { checkToken, getUserInfo } from '../actions/auth'

export default (ChildComponent) => {
    class AuthComponent extends Component {
        componentDidMount() {
            this.shouldNavigateToLogin();
        }

        componentDidUpdate() {
            this.shouldNavigateToLogin();
        }

        /* 
            if token not exist, direct to sign in page,
            f token exist, verify token 
        */
        shouldNavigateToLogin() {
            if (!this.props.auth.token) {
                this.props.history.push('/signin');
            } else {
                if (!this.props.auth.authentication) {
                    this.props.checkToken(
                        () => {
                            this.props.history.push('/signin');
                        },
                    );
                }
            }
        }

        render() {
            return (
                <div>
                    {this.props.auth.authentication ? <ChildComponent {...this.props} /> : null}
                </div>

            )

        }
    }

    function mapStateToProps(state) {
        return {
            auth: state.auth
        }
    }

    return connect(mapStateToProps, { checkToken, getUserInfo })(AuthComponent);
}