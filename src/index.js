import React from "react";
import ReactDOM from "react-dom";
import Root from "./Root";
import Router from "./routers";
import "./i18n";
import "./styles/styles.scss";
import * as serviceWorker from "./serviceWorker";
import ReactGA from "react-ga";

ReactGA.initialize("UA-108432981-2");
ReactGA.pageview(window.location.pathname + window.location.search);

ReactDOM.render(
  <Root>
    <Router />
  </Root>,
  document.getElementById("root")
);
serviceWorker.unregister();
