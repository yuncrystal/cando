import React, { Suspense } from 'react';
import { Provider } from 'react-redux';
import configureStore from './configureStore';

const store = configureStore();

export default (props) => {
    return (
        <Provider store={store}>
            <Suspense fallback=''>  
                {props.children}
            </Suspense>

        </Provider>
    )
}