import { SING_IN, AUTH_ERROR, CHECK_TOKEN, USER_INFO, LOGOUT } from '../types/auth'
import { TOKEN } from '../types/common';

const initState = {
    authentication: false,
    token: localStorage.getItem(TOKEN),
    account: null
}

export default function (state = initState, action) {
    switch (action.type) {
        case SING_IN:
            return { ...state, token: action.payload, authentication: true }
        case CHECK_TOKEN:
            return { ...state, authentication: true }
        case USER_INFO:
            return { ...state, account: action.payload }

        case LOGOUT:
            return { initState }

        case AUTH_ERROR:
            return { ...state, authentication: false, token: null }
        default:
            return initState
    }
}