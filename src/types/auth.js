export const SING_IN = 'sign_in'
export const SING_UP = 'sign_up'
export const LOGOUT = 'logout'

export const USER_INFO = 'user_info'
export const AUTH_ERROR = 'auth_error'

export const CHECK_TOKEN ='check_token'