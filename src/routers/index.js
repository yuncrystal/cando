import React from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';
import Header from '../pages/cando-header';
import Footer from '../pages/cando-header/footer';
import Homepage from '../pages/cando-home';
import introduce from '../pages/cando-introduce';
import TraceAndManage from '../pages/cando-trace';
import PlaceOrder from '../pages/cando-order/NewOrder';
import Signin from '../pages/cando-auth/signin';
import SignUp from '../pages/cando-auth/signUp';
import NoMatch from '../pages/cando-error/error';
import UserCenter from '../pages/cando-center/userCenter';
import ForgetPassword from '../pages/cando-auth/forgetPassword';
import NewOrder from '../pages/cando-order/NewOrder';
import OrderDetail from '../pages/cando-trace/OrderDetail';
import Terms from '../pages/cando-term/Terms';
import introduceSampleOrder from '../pages/cando-home/introduce/sample-order'
import introduceExpressType from '../pages/cando-home/introduce/express-type'
import introduceDestination from '../pages/cando-home/introduce/destination'
import introduceRemoteRegion from '../pages/cando-home/introduce/coverRemoteRegion'
import introduceCurrentExpress from '../pages/cando-home/introduce/current-Express'

class Index extends React.Component {

    render() {
        return (
            <HashRouter>
                <div>
                    <Header />
                    <Switch>
                        <Route path="/" exact component={Homepage} />
                        <Route path="/introduce" exact component={introduce} />
                        <Route path="/trace_manage" component={TraceAndManage} />
                        <Route path="/order/:id" component={OrderDetail} />
                        <Route path="/place_order" exact component={PlaceOrder} />
                        <Route path="/signin" exact component={Signin} />
                        <Route path="/signup" exact component={SignUp} />
                        <Route path="/my-center" component={UserCenter} />
                        <Route path="/forget_password" exact component={ForgetPassword} />
                        <Route path="/new_order" exact component={NewOrder} />
                        <Route path="/terms" exact component={Terms} />
                        <Route path="/homeSampleOrderIntroduce" exact component={introduceSampleOrder} />
                        <Route path='/homeIntroduceExpressType' exact component={introduceExpressType} />
                        <Route path='/homeIntroduceDestination' exact component={introduceDestination} />
                        <Route path='/homeIntroduceRemoteRegion' exact component={introduceRemoteRegion} />
                        <Route path='/homeIntroduceCurrentExpress' exact component={introduceCurrentExpress} />
                        <Route component={NoMatch} />
                    </Switch>
                </div>


                <Footer />
            </HashRouter>
        )
    }

}

/**Home页面各个模块介绍页面：{homeSampleOrderIntroduce、homeIntroduceExpressType、homeIntroduceDestination、homeIntroduceRemoteRegion、homeIntroduceCurrentExpress}
            因Home原页面没有引入路由，为了减少代码入侵，所以在Routers里把所有介绍页注入。
            后期重新改写Cando时优化该部分：需将以下页面注册迁移到Home页*/


export default Index

