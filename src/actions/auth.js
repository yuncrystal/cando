import { SING_IN,AUTH_ERROR,CHECK_TOKEN,USER_INFO,LOGOUT} from '../types/auth';
import { TOKEN } from '../types/common';
import candoAPI from '../api/candoAPI';
import {API_URL} from '../api/apiUrl';

export const register = ({username,password},successCallback,errorCallback) => async dispatch =>{
    
    // localStorage.setItem(TOKEN, response.data.token);
}

export const signin = ({username,password},successCallback,errorCallback)=> async dispatch =>{
    try {
       
        const response =  await candoAPI.post(API_URL.AUTH.SIGNIN,{username,password})
        localStorage.setItem(TOKEN, response.data.token);
        candoAPI.defaults.headers.common['Authorization'] = 'jwt '+response.data.token;
        dispatch ({
            type:SING_IN,
            payload:response.data.token
        })
        successCallback();
    }catch (e) {
        dispatch ({
            type:AUTH_ERROR,
        });
        errorCallback();
    }
    
}



export const checkToken = (errorCallback)=> async dispatch =>{
    try {
        await candoAPI.post(API_URL.AUTH.CHECK_TOKEN,{'token':localStorage.getItem(TOKEN)});
        dispatch ({
            type:CHECK_TOKEN,
        })
        candoAPI.defaults.headers.common['Authorization'] = 'jwt '+localStorage.getItem(TOKEN);
    }catch (e) {
        dispatch ({
            type:AUTH_ERROR,
        });
        localStorage.removeItem(TOKEN);
        errorCallback();
    }
    
}

export const getUserInfo = () => async dispatch =>{
    try {
        const response =  await candoAPI.get(API_URL.AUTH.USER_INFO);
        dispatch ({
            type:USER_INFO,
            payload:response.data
        })
    }catch (e) {
        console.log('get user Info error');
    }
}

export const logout = () => async dispatch =>{
    dispatch ({
        type:LOGOUT,
    })
    localStorage.removeItem(TOKEN);
}


