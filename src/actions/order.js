import { TOKEN } from '../types/common';
import candoAPI from '../api/candoAPI';
import { API_URL } from '../api/apiUrl';

export const getAllOrder = async () => {
    try {
        const response = await candoAPI.get(API_URL.ORDER.ALL_ORDER);
        return response;
    } catch (err) {
        return Promise.reject();
    }
}